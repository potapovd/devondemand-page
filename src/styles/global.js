import { createGlobalStyle } from "styled-components"
import reset from "styled-reset"

import GTWalsheimProRegWoff2 from "../fonts/GTWalsheimPro-Regular.woff2";
import GTWalsheimProRegWoff from "../fonts/GTWalsheimPro-Regular.woff";
import GTWalsheimProRegTtf from "../fonts/GTWalsheimPro-Regular.ttf";

import GTWalsheimProBoldWoff2 from "../fonts/GTWalsheimPro-Bold.woff2";
import GTWalsheimProBoldWoff from "../fonts/GTWalsheimPro-Bold.woff";
import GTWalsheimProBoldTtf from "../fonts/GTWalsheimPro-Bold.ttf";

import GTWalsheimProBlackWoff2 from "../fonts/GTWalsheimPro-Black.woff2";
import GTWalsheimProBlackWoff from "../fonts/GTWalsheimPro-Black.woff";
import GTWalsheimProBlackTtf from "../fonts/GTWalsheimPro-Black.ttf";

import GTWalsheimProMediumWoff2 from "../fonts/GTWalsheimPro-Medium.woff2";
import GTWalsheimProMediumWoff from "../fonts/GTWalsheimPro-Medium.woff";
import GTWalsheimProMediumTtf from "../fonts/GTWalsheimPro-Medium.ttf";

const GlobalStyle = createGlobalStyle`
  ${ reset }
    
  @font-face {
      font-family: "GT Walsheim Pro", sans-serif;
      src: local('GT Walsheim Pro Regular'),
      local('GT-Walsheim-Pro-Regular'),
      url(${ GTWalsheimProRegWoff2 }) format('woff2'),
      url(${ GTWalsheimProRegWoff }) format('woff'),
      url(${ GTWalsheimProRegTtf }) format('truetype');
      font-weight: 400;
      font-style: normal;
  }
  
  @font-face {
      font-family: 'GT Walsheim Pro Bold';
      src: local('GT Walsheim Pro Bold'),
      local('GT-Walsheim-Pro-Bold'),
      url(${ GTWalsheimProBoldWoff2 }) format('woff2'),
      url(${ GTWalsheimProBoldWoff }) format('woff'),
      url(${ GTWalsheimProBoldTtf }) format('truetype');
      font-weight: 400;
      font-style: normal;
  }
  
  @font-face {
      font-family: 'GT Walsheim Pro Black';
      src: local('GT Walsheim Pro Black'),
      local('GT-Walsheim-Pro-Black'),
      url(${ GTWalsheimProBlackWoff2 }) format('woff2'),
      url(${ GTWalsheimProBlackWoff }) format('woff'),
      url(${ GTWalsheimProBlackTtf }) format('truetype');
      font-weight: 400;
      font-style: normal;
  }
  
  @font-face {
      font-family: 'GT Walsheim Pro Medium';
      src: local('GT Walsheim Pro Medium'),
      local('GT-Walsheim-Pro-Medium'),
      url(${ GTWalsheimProMediumWoff2 }) format('woff2'),
      url(${ GTWalsheimProMediumWoff }) format('woff'),
      url(${ GTWalsheimProMediumTtf }) format('truetype');
      font-weight: 400;
      font-style: normal;
  }
  
  *,
  *::before,
  *::after {
      box-sizing: border-box;
  }
  
   :root {
      -moz-tab-size: 4;
      tab-size: 4;
  }
  
   ::-webkit-inner-spin-button,
   ::-webkit-outer-spin-button {
      height: auto;
  }
  
  html {
      box-sizing: border-box;
      scroll-behavior: smooth;
      overflow-x: hidden;
      overflow-y: visible;
      line-height: 1.15;
      -webkit-text-size-adjust: 100%;
      ms-overflow-style: scrollbar;
      -webkit-text-size-adjust: 100%;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
  }
  
  body {
      font-family: "GT Walsheim Pro", sans-serif;
      font-size: 12px;
      color: #000;
      background-color: #FAFAFA;
      margin: 0;
      overflow-x: hidden;
      overflow-y: visible;
  }
  
  button,
  input,
  optgroup,
  select,
  textarea {
      font-family: inherit;
      font-size: 100%;
      line-height: 1.58;
      margin: 0;
  }
  
  button,
  [type="button"],
  [type="reset"],
  [type="submit"] {
      -webkit-appearance: button;
  }
  
  button::-moz-focus-inner,
  [type="button"]::-moz-focus-inner,
  [type="reset"]::-moz-focus-inner,
  [type="submit"]::-moz-focus-inner {
      border-style: none;
      padding: 0;
  }
  
  
  @keyframes modal-video{from{opacity:0}to{opacity:1}}@keyframes modal-video-inner{from{transform:translate(0, 100px)}to{transform:translate(0, 0)}}.modal-video{position:fixed;top:0;left:0;width:100%;height:100%;background-color:rgba(0,0,0,0.5);z-index:1000000;cursor:pointer;opacity:1;animation-timing-function:ease-out;animation-duration:.3s;animation-name:modal-video;-webkit-transition:opacity .3s ease-out;-moz-transition:opacity .3s ease-out;-ms-transition:opacity .3s ease-out;-o-transition:opacity .3s ease-out;transition:opacity .3s ease-out}.modal-video-effect-exit{opacity:0}.modal-video-effect-exit .modal-video-movie-wrap{-webkit-transform:translate(0, 100px);-moz-transform:translate(0, 100px);-ms-transform:translate(0, 100px);-o-transform:translate(0, 100px);transform:translate(0, 100px)}.modal-video-body{max-width:940px;width:100%;height:100%;margin:0 auto;display:table}.modal-video-inner{display:table-cell;vertical-align:middle;width:100%;height:100%}.modal-video-movie-wrap{width:100%;height:0;position:relative;padding-bottom:56.25%;background-color:#333;animation-timing-function:ease-out;animation-duration:.3s;animation-name:modal-video-inner;-webkit-transform:translate(0, 0);-moz-transform:translate(0, 0);-ms-transform:translate(0, 0);-o-transform:translate(0, 0);transform:translate(0, 0);-webkit-transition:-webkit-transform .3s ease-out;-moz-transition:-moz-transform .3s ease-out;-ms-transition:-ms-transform .3s ease-out;-o-transition:-o-transform .3s ease-out;transition:transform .3s ease-out}.modal-video-movie-wrap iframe{position:absolute;top:0;left:0;width:100%;height:100%}.modal-video-close-btn{position:absolute;z-index:2;top:-35px;right:-35px;display:inline-block;width:35px;height:35px;overflow:hidden;border:none;background:transparent}.modal-video-close-btn:before{transform:rotate(45deg)}.modal-video-close-btn:after{transform:rotate(-45deg)}.modal-video-close-btn:before,.modal-video-close-btn:after{content:'';position:absolute;height:2px;width:100%;top:50%;left:0;margin-top:-1px;background:#fff;border-radius:5px;margin-top:-6px}


`;

export default GlobalStyle;