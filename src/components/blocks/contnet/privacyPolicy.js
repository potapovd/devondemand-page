import React from "react"
import styled from "styled-components"

import { Container, Typography } from "@material-ui/core"

export default function PrivacyPolicyPage() {
    return (
        <ContainerBox>
            <TypographyH1 variant={ "h1" }>Privacy Policy</TypographyH1>
            <TypographyDate variant={ "body1" }>Updated 29/07/2020</TypographyDate>
            <TypographyH2 variant={ "h2" }>1. Introduction</TypographyH2>
            <TypographyText variant={ "body2" }>
                Welcome to BCL
                <br/><br/>BCL (<b>'us', 'we', or 'our'</b>) operates DevOnDemand.is (hereinafter referred to as
                'Service').
                <br/><br/>Our Privacy Policy governs your visit to DevOnDemand.is and app.DevOnDemand.is, and explains
                how we collect, safeguard and disclose information that results from your use of our Service.
                <br/><br/>We use your data to provide and improve Service. By using Service, you agree to the collection
                and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy,
                the terms used in this Privacy Policy have the same meanings as in our Terms and Conditions.
                <br/><br/>Our Terms and Conditions (<b>'Terms'</b>) govern all use of our Service and together with the
                Privacy
                Policy constitutes your agreement with us (<b>'agreement'</b>).
            </TypographyText>


            <TypographyH2 variant={ "h1" }>2. Definitions</TypographyH2>
            <TypographyText variant={ "body2" }>
                <b>SERVICE</b> means the DevOnDemand.is and app.DevOnDemand.is website operated by BCL
                <br/><br/><b>PERSONAL DATA</b> means data about a living individual who can be identified from those
                data (or
                from those and other information either in our possession or likely to come into our possession).
                <br/><br/><b>USAGE DATA</b> is data collected automatically either generated by the use of Service or
                from Service infrastructure itself (for example, the duration of a page visit).
                <br/><br/><b>COOKIES</b> are small files stored on your device (computer or mobile device).
                <br/><br/> <b>DATA CONTROLLER</b> means a natural or legal person who (either alone or jointly or in
                common
                with other persons) determines the purposes for which and the manner in which any personal data are, or
                are to be,
                processed. For the purpose of this Privacy Policy, we are a Data Controller of your data.
                <br/><br/><b>DATA PROCESSORS (OR SERVICE PROVIDERS)</b> means any natural or legal person who processes
                the
                data on behalf of the Data Controller. We may use the services of various Service Providers in order to
                process your data more effectively.
                <br/><br/><b>DATA SUBJECT</b> is any living individual who is the subject of Personal Data.
                <br/><br/><b> THE USER</b> is the individual using our Service. The User corresponds to the Data
                Subject, who is the subject of Personal Data.
            </TypographyText>


            <TypographyH2 variant={ "h1" }>3. Information Collection and Use</TypographyH2>
            <TypographyText variant={ "body2" }>
                We collect several different types of information for various purposes to provide and improve our
                Service to
                you.
            </TypographyText>


            <TypographyH2 variant={ "h1" }>4. Types of Data Collected</TypographyH2>
            <TypographyText variant={ "body2" }>
                <b>Personal Data</b>
                <br/>While using our Service, we may ask you to provide us with certain personally identifiable
                information
                that can
                be used to contact or identify you ('Personal Data'). Personally identifiable information may include,
                but is
                not limited to:
                <ol>
                    <li>Email address</li>
                    <li>First name and last name</li>
                    <li>Phone number</li>
                    <li>Cookies and Usage Data</li>
                </ol>
                <br/>We may use your Personal Data to contact you with newsletters, marketing or promotional
                materials and
                other information that may be of interest to you. You may opt out of receiving any, or all, of these
                communications from us by following the unsubscribe link.
                <br/><br/><b> Usage Data</b>
                <br/>We may also collect information that your browser sends whenever you visit our Service or when
                you access Service by or through a mobile device (<b>'Usage Data'</b>).
                <br/><br/>This Usage Data may include information such as your computer's Internet Protocol address
                (e.g. IP
                address), browser type, browser version, the pages of our Service that you visit, the time and date of
                your
                visit, the time spent on those pages, unique device identifiers and other diagnostic data.
                <br/><br/>When you access Service with a mobile device, this Usage Data may include information such as
                the type
                of mobile device you use, your mobile device unique ID, the IP address of your mobile device, your
                mobile
                operating system, the type of mobile Internet browser you use, unique device identifiers and other
                diagnostic
                data.<br/><br/>
                <b>Tracking Cookies Data</b>
                <br/>We use cookies and similar tracking technologies to track the activity on our Service and we
                hold
                certain information.
                <br/><br/>Cookies are files with a small amount of data which may include an anonymous unique
                identifier.
                Cookies are sent to your browser from a website and stored on your device. Other tracking technologies
                are also
                used such as beacons, tags and scripts to collect and track information and to improve and analyze our
                Service.
                <br/><br/>You can instruct your browser to refuse all cookies or to indicate when a cookie is being
                sent.
                However, if you do not accept cookies, you may not be able to use some portions of our Service.
                <br/><br/>Examples of Cookies we use:
                <ol>
                    <li><b>Session Cookies:</b> We use Session Cookies to operate our Service.</li>
                    <li><b>Preference Cookies:</b> We use Preference Cookies to remember your preferences and various
                        settings.
                    </li>
                    <li><b>Security Cookies:</b> We use Security Cookies for security purposes.</li>
                    <li><b>Advertising Cookies:</b> Advertising Cookies are used to serve you with advertisements that
                        may
                        be relevant to you and your interests.
                    </li>
                </ol><br/>
                <b>Other Data</b><br/>
                While using our Service, we may also collect the following information: sex, age, date of birth, place
                of birth,
                passport details, citizenship, registration at place of residence and actual address, telephone number
                (work,
                mobile), details of documents on education, qualification, professional training, employment agreements,
                non-disclosure agreements, information on bonuses and compensation, information on marital status,
                family
                members, social security (or other taxpayer identification) number, office location and other data.
            </TypographyText>

            <TypographyH2 variant={ "h1" }> 5. Use of Data</TypographyH2>
            <TypographyText variant={ "body2" }>
                <ol>
                    <li>BCL uses the collected data for various purposes:</li>
                    <li>to provide and maintain our Service;</li>
                    <li>to notify you about changes to our Service;</li>
                    <li>to allow you to participate in interactive features of our Service when you choose to do so;
                    </li>
                    <li>to provide customer support;</li>
                    <li>to gather analysis or valuable information so that we can improve our Service;</li>
                    <li>to monitor the usage of our Service;</li>
                    <li>to detect, prevent and address technical issues;</li>
                    <li>to fulfill any other purpose for which you provide it;</li>
                    <li>to carry out our obligations and enforce our rights arising from any contracts entered into
                        between you
                        and us, including for billing and collection;
                    </li>
                    <li>to provide you with notices about your account and/or subscription, including expiration and
                        renewal
                        notices, email-instructions, etc.;
                    </li>
                    <li>to provide you with news, special offers and general information about other goods, services and
                        events
                        which we offer that are similar to those that you have already purchased or enquired about
                        unless you
                        have opted not to receive such information;
                    </li>
                    <li>in any other way we may describe when you provide the information;</li>
                    <li>for any other purpose with your consent.</li>
                </ol>
            </TypographyText>


            <TypographyH2 variant={ "h1" }>6.Retention of Data</TypographyH2>
            <TypographyText variant={ "body2" }>
                We will retain your Personal Data only for as long as is necessary for the purposes set out in this
                Privacy
                Policy. We will retain and use your Personal Data to the extent necessary to comply with our legal
                obligations
                (for example, if we are required to retain your data to comply with applicable laws), resolve disputes,
                and
                enforce our legal agreements and policies.
                <br/><br/>We will also retain Usage Data for internal analysis purposes. Usage Data is generally
                retained for a
                shorter period, except when this data is used to strengthen the security or to improve the functionality
                of our
                Service, or we are legally obligated to retain this data for longer time periods.
            </TypographyText>

            <TypographyH2 variant={ "h1" }> 7. Transfer of Data</TypographyH2>
            <TypographyText variant={ "body2" }>
                Your information, including Personal Data, may be transferred to – and maintained on – computers located
                outside
                of your state, province, country or other governmental jurisdiction where the data protection laws may
                differ
                from those of your jurisdiction.
                <br/><br/>If you are located outside United States and choose to provide information to us, please note
                that we
                transfer the data, including Personal Data, to United States and process it there.
                <br/><br/>Your consent to this Privacy Policy followed by your submission of such information represents
                your
                agreement to that transfer.
                <br/><br/>BCL will take all the steps reasonably necessary to ensure that your data is treated securely
                and in
                accordance with this Privacy Policy and no transfer of your Personal Data will take place to an
                organisation or
                a country unless there are adequate controls in place including the security of your data and other
                personal
                information.
            </TypographyText>

            <TypographyH2 variant={ "h1" }>8. Disclosure of Data</TypographyH2>
            <TypographyText variant={ "body2" }>
                We may disclose personal information that we collect, or you provide:
                <br/><br/><b>1.Disclosure for Law Enforcement.</b>
                <br/>Under certain circumstances, we may be required to disclose your Personal Data if required to
                do so by
                law or in response to valid requests by public authorities.
                <br/><br/><b>2.Business Transaction.</b>
                <br/>If we or our subsidiaries are involved in a merger, acquisition or asset sale, your Personal
                Data may
                be transferred.
                <br/><br/><b>3.Other cases. We may disclose your information also:</b><br/>
                <ol>
                    <li>to our subsidiaries and affiliates;</li>
                    <li>to contractors, service providers, and other third parties we use to support our business;</li>
                    <li>to fulfill the purpose for which you provide it;</li>
                    <li>for the purpose of including your company’s logo on our website;</li>
                    <li>for any other purpose disclosed by us when you provide the information;</li>
                    <li>if we believe disclosure is necessary or appropriate to protect the rights, property, or safety
                        of the
                        Company, our customers, or others.
                    </li>
                </ol>
            </TypographyText>


            <TypographyH2 variant={ "h1" }>9. Security of Data</TypographyH2>
            <TypographyText variant={ "body2" }>The security of your data is important to us but remember that no method
                of transmission over the Internet or method of electronic storage is 100% secure. While we strive to use
                commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute
                security.</TypographyText>


            <TypographyH2 variant={ "h1" }>10. Your Data Protection Rights Under General Data Protection Regulation
                (GDPR)</TypographyH2>
            <TypographyText variant={ "body2" }>
                If you are a resident of the European Union (EU) and European Economic Area (EEA), you have certain data
                protection rights, covered by GDPR. – See more at <a
                href="https://eur-lex.europa.eu/eli/reg/2016/679/oj">https://eur-lex.europa.eu/eli/reg/2016/679/oj</a>
                <br/><br/>We aim to take reasonable steps to allow you to correct, amend, delete, or limit the use of
                your
                Personal Data.
                <br/><br/>If you wish to be informed what Personal Data we hold about you and if you want it to be
                removed from
                our systems, please email us at <a href="devondemandco@gmail.com">devondemandco@gmail.com</a>
                <br/><br/>In certain circumstances, you have the following data protection rights:
                <ol>
                    <li>the right to access, update or to delete the information we have on you;</li>
                    <li>the right of rectification. You have the right to have your information rectified if that
                        information is inaccurate or incomplete;
                    </li>
                    <li>the right to object. You have the right to object to our processing of your Personal
                        Data;
                    </li>
                    <li>the right of restriction. You have the right to request that we restrict the
                        processing of
                        your personal information;
                    </li>
                    <li>the right to data portability. You have the right to be provided with a copy of
                        your
                        Personal Data in a structured, machine-readable and commonly used format;
                    </li>
                    <li>the right to withdraw consent. You also have the right to withdraw your
                        consent at
                        any time where we rely on your consent to process your personal information;
                    </li>
                </ol>
                <br/>Please note that we may ask you to verify your identity before responding to such requests.
                Please
                note, we may not able to provide Service without some necessary data.
                <br/><br/>You have the right to complain to a Data Protection Authority about our collection and use of
                your
                Personal Data. For more information, please contact your local data protection authority in the European
                Economic Area (EEA).
            </TypographyText>

            <TypographyH2 variant={ "h1" }>11. Your Data Protection Rights under the California Privacy Protection Act
                (CalOPPA)</TypographyH2>
            <TypographyText variant={ "body2" }>
                CalOPPA is the first state law in the nation to require commercial websites and online services to post
                a
                privacy policy. The law’s reach stretches well beyond California to require a person or company in the
                United
                States (and conceivable the world) that operates websites collecting personally identifiable information
                from
                California consumers to post a conspicuous privacy policy on its website stating exactly the information
                being
                collected and those individuals with whom it is being shared, and to comply with this policy. – See more
                at:
                <a href="https://consumercal.org/about-cfc/cfc-education-foundation/california-online-privacy-protection-act-caloppa-3/">
                    https://consumercal.org/about-cfc/cfc-education-foundation/california-online-privacy-protection-act-caloppa-3/
                </a>
                <br/><br/>According to CalOPPA we agree to the following:
                <ol>
                    <li>users can visit our site anonymously;</li>
                    <li>our Privacy Policy link includes the word 'Privacy', and can easily be found on the page
                        specified above
                        on the home page of our website;
                    </li>
                    <li>users will be notified of any privacy policy changes on our Privacy Policy Page;</li>
                    <li>users are able to change their personal information by emailing us at <a
                        href="devondemandco@gmail.com">devondemandco@gmail.com.</a>
                    </li>
                </ol>
                <br/>Our Policy on 'Do Not Track' Signals:
                <br/><br/>We honor Do Not Track signals and do not track, plant cookies, or use advertising when a Do
                Not Track
                browser mechanism is in place. Do Not Track is a preference you can set in your web browser to inform
                websites
                that you do not want to be tracked.
                <br/><br/>You can enable or disable Do Not Track by visiting the Preferences or Settings page of your
                web
                browser.
            </TypographyText>


            <TypographyH2 variant={ "h1" }>12. Service Providers</TypographyH2>
            <TypographyText variant={ "body2" }>
                We may employ third party companies and individuals to facilitate our Service (<b>'Service
                Providers'</b>),
                provide Service on our behalf, perform Service-related services or assist us in analysing how our
                Service is
                used.
                <br/><br/>These third parties have access to your Personal Data only to perform these tasks on our
                behalf and
                are obligated not to disclose or use it for any other purpose.</TypographyText>

            <TypographyH2 variant={ "h1" }>13. Analytics</TypographyH2>
            <TypographyText variant={ "body2" }>
                We may use third-party Service Providers to monitor and analyze the use of our Service.
                <b>Google Analytics</b>
                <br/><br/>Google Analytics is a web analytics service offered by Google that tracks and reports website
                traffic.
                Google uses the data collected to track and monitor the use of our Service. This data is shared with
                other
                Google services. Google may use the collected data to contextualise and personalise the ads of its own
                advertising network.
                <br/><br/>For more information on the privacy practices of Google, please visit the Google Privacy Terms
                web
                page: <a href="https://policies.google.com/privacy?hl=en">
                https://policies.google.com/privacy?hl=en
            </a>
                <br/><br/>We also encourage you to review the Google's policy for safeguarding your data:
                <a href=" https://support.google.com/analytics/answer/6004245"> https://support.google.com/analytics/answer/6004245</a>.
                <br/><br/><b>Cloudflare analytics</b>
                <br/>Cloudflare analytics is a web analytics service operated by Cloudflare Inc. Read the Privacy Policy
                here:
                <a href="https://www.cloudflare.com/privacypolicy/">https://www.cloudflare.com/privacypolicy/</a>
            </TypographyText>

            <TypographyH2 variant={ "h1" }>14. CI/CD tools</TypographyH2>
            <TypographyText variant={ "body2" }>
                We may use third-party Service Providers to automate the development process of our Service.
                <b>GitHub</b>
                GitHub is provided by GitHub, Inc.
                <br/><br/>GitHub is a development platform to host and review code, manage projects, and build software.
                <br/><br/>For more information on what data GitHub collects for what purpose and how the protection of
                the data
                is ensured, please visit GitHub Privacy Policy page:
                <a href="https://help.github.com/en/articles/github-privacy-statement">https://help.github.com/en/articles/github-privacy-statement</a>.
            </TypographyText>


            <TypographyH2 variant={ "h1" }>15. Behavioral Remarketing</TypographyH2>
            <TypographyText variant={ "body2" }>
                BCL uses remarketing services to advertise on third party websites to you after you visited our Service.
                We and
                our third-party vendors use cookies to inform, optimise and serve ads based on your past visits to our
                Service.
                <br/><br/><b>Google Ads (AdWords)</b>
                <br/>Google Ads (AdWords) remarketing service is provided by Google Inc.
                <br/><br/>You can opt-out of Google Analytics for Display Advertising and customise the Google Display
                Network
                ads by visiting the Google Ads Settings page: <a
                href="http://www.google.com/settings/ads">http://www.google.com/settings/ads</a>
                <br/><br/>Google also recommends installing the Google Analytics Opt-out Browser Add-on –
                <a href="https://tools.google.com/dlpage/gaoptout">https://tools.google.com/dlpage/gaoptout</a> – for
                your web browser. Google Analytics Opt-out Browser Add-on
                provides visitors with the ability to prevent their data from being collected and used by Google
                Analytics.
                <br/>For more information on the privacy practices of Google, please visit the Google Privacy Terms
                web page: <a
                href="https://policies.google.com/privacy?hl=en">https://policies.google.com/privacy?hl=en</a>
                <br/><br/><b>Twitter</b>
                <br/>Twitter remarketing service is provided by Twitter Inc.
                <br/><br/>You can opt-out from Twitter's interest-based ads by following their instructions:
                <a href="https://support.twitter.com/articles/20170405">https://support.twitter.com/articles/20170405</a>
                <br/><br/>You can learn more about the privacy practices and policies of Twitter by visiting their
                Privacy Policy page:<a href=" https://twitter.com/privacy"> https://twitter.com/privacy</a>
                <br/><br/><b>Facebook</b>
                <br/>Facebook remarketing service is provided by Facebook Inc.
                <br/><br/>You can learn more about interest-based advertising from Facebook by visiting this page:
                <a href="https://www.facebook.com/help/164968693837950">https://www.facebook.com/help/164968693837950</a>
                <br/><br/>To opt-out from Facebook's interest-based ads, follow these instructions from Facebook:
                <a href="https://www.facebook.com/help/568137493302217">https://www.facebook.com/help/568137493302217</a>
                <br/><br/>Facebook adheres to the Self-Regulatory Principles for Online Behavioural Advertising
                established by the Digital Advertising Alliance. You can also opt-out from Facebook and other
                participating companies
                through the Digital Advertising Alliance in the USA <a
                href="http://www.aboutads.info/choices/">http://www.aboutads.info/choices/</a>, the Digital Advertising
                Alliance of Canada in Canada <a href="http://youradchoices.ca/ ">http://youradchoices.ca/ </a> or the
                European Interactive Digital Advertising Alliance in
                Europe
                <a href="http://www.youronlinechoices.eu/">
                    http://www.youronlinechoices.eu/
                </a>, or opt-out using your mobile device settings.
                <br/><br/>For more information on the privacy practices of Facebook, please visit Facebook's Data
                Policy:
                <a href="https://www.facebook.com/privacy/explanation">https://www.facebook.com/privacy/explanation</a>
                <br/><br/><b>Pinterest</b>
                <br/>Pinterest remarketing service is provided by Pinterest Inc.
                <br/><br/>You can opt-out from Pinterest's interest-based ads by enabling the 'Do Not Track'
                functionality of your web browser or by following Pinterest instructions:
                <a href="http://help.pinterest.com/en/articles/personalization-and-data">http://help.pinterest.com/en/articles/personalization-and-data</a>
                <br/><br/>You can learn more about the privacy practices and policies of Pinterest by visiting their
                Privacy Policy page: <a
                href="https://about.pinterest.com/en/privacy-policy">https://about.pinterest.com/en/privacy-policy</a>
            </TypographyText>

            <TypographyH2 variant={ "h1" }>16.Payments</TypographyH2>
            <TypographyText variant={ "body2" }>
                We may provide paid products and/or services within Service. In that case, we use third-party services
                for
                payment processing (e.g. payment processors).
                <br/><br/>We will not store or collect your payment card details. That information is provided directly
                to our
                third-party payment processors whose use of your personal information is governed by their Privacy
                Policy. These
                payment processors adhere to the standards set by PCI-DSS as managed by the PCI Security Standards
                Council,
                which is a joint effort of brands like Visa, Mastercard, American Express and Discover. PCI-DSS
                requirements
                help ensure the secure handling of payment information.
                The payment processors we work with are:
                <br/><b>Stripe:</b>
                Their Privacy Policy can be viewed at: <a
                href="https://stripe.com/us/privacy">https://stripe.com/us/privacy</a>
            </TypographyText>

            <TypographyH2 variant={ "h1" }>17.Links to Other Sites</TypographyH2>
            <TypographyText variant={ "body2" }>
                Our Service may contain links to other sites that are not operated by us. If you click a third party
                link, you
                will be directed to that third party's site. We strongly advise you to review the Privacy Policy of
                every site
                you visit.
                <br/><br/>We have no control over and assume no responsibility for the content, privacy policies or
                practices of
                any third party sites or services.
            </TypographyText>


            <TypographyH2 variant={ "h1" }>18. Children's Privacy</TypographyH2>
            <TypographyText variant={ "body2" }>
                Our Services are not intended for use by children under the age of 18 ('Child' or 'Children').
                <br/><br/>We do not knowingly collect personally identifiable information from Children under 18. If you
                become
                aware that a Child has provided us with Personal Data, please contact us. If we become aware that we
                have
                collected Personal Data from Children without verification of parental consent, we take steps to remove
                that
                information from our servers.
            </TypographyText>

            <TypographyH2 variant={ "h1" }>19. Changes to This Privacy Policy</TypographyH2>
            <TypographyText variant={ "body2" }>
                We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new
                Privacy
                Policy on this page.
                <br/><br/>We will let you know via email and/or a prominent notice on our Service, prior to the change
                becoming
                effective and update 'effective date' at the top of this Privacy Policy.
                <br/><br/>You are advised to review this Privacy Policy periodically for any changes. Changes to this
                Privacy
                Policy are effective when they are posted on this page.
            </TypographyText>


            <TypographyH2 variant={ "h1" }> 20. Contact Us</TypographyH2>
            <TypographyText variant={ "body2" }>
                If you have any questions about this Privacy Policy, please contact us:
                <br/>By email: <a href='mailto:devondemandco@gmail.com'>devondemandco@gmail.com</a>
            </TypographyText>

        </ContainerBox>

    )
}


const ContainerBox = styled(Container)`
  margin: 70px auto;
`;
const TypographyH1 = styled(Typography)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 24px;
    line-height: 1.3em;
    letter-spacing: 0.03em;
    @media (max-width: 768px) {
        font-size: 16px;
    }
`;
const TypographyDate = styled(Typography)`
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    line-height: 1.3em;
    margin: 20px 0;
    letter-spacing: 0.03em;
    @media (max-width: 768px) {
        font-size: 10px;
    }
`;
const TypographyH2 = styled(Typography)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 18px;
    line-height: 1.3em;
    letter-spacing: 0.03em;
    margin:20px 0  10px;
    @media (max-width: 768px) {
        font-size: 10px;
    }
`;
const TypographyText = styled(Typography)`
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 16px;
    line-height: 1.5em;
    letter-spacing: 0.03em;
    & a{
        color: #000;
    }
    & b{
        font-family: "GT Walsheim Pro Bold", sans-serif;
    }
    & ol{
      margin-left: 40px;
      list-style-type: decimal;
    }
    @media (max-width: 768px) {
        font-size: 10px;
    }
`;