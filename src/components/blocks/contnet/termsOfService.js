import React from "react"
import styled from "styled-components"

import { Container, Typography } from "@material-ui/core"

export default function TermsOfServicePage() {
    return (
        <ContainerBox>
            <TypographyH1 variant={ "h1" }>Terms of Service</TypographyH1>
            <TypographyDate variant={ "body1" }>Updated 29/07/2020</TypographyDate>
            <TypographyH2 variant={ "h2" }>1. Introduction</TypographyH2>
            <TypographyText variant={ "body2" }>
                Welcome to <b> BCL (“Company”, “we”, “our”, “us”)</b>! As you have just clicked to our Terms of Service,
                please make a pause, grab a cup of coffee and carefully read the following pages. It will take you
                approximately 20 minutes.
                <br/><br/>These Terms of Service (<b>“Terms”, “Terms of Service”</b>) govern your use of our web pages
                located at <a href="https://devondemand.co"> devondemand.co</a> is operated by BCL.
                <br/><br/>Your agreement with us includes these Terms and our Privacy Policy (<b>“Agreements”</b>). You
                acknowledge that you have read and understood Agreements, and agree to be bound of them.
                <br/><br/>If you do not agree with (or cannot comply with) Agreements, then you may not use the Service,
                but please let us know by emailing at <a
                href="mailto:devondemandco@gmail.com">devondemandco@gmail.com</a> so we can try to find a solution.
                These Terms apply to all visitors, users and others who wish to access or use Service.
                Thank you for being responsible.
            </TypographyText>

            <TypographyH2 variant={ "h2" }>2. Communications</TypographyH2>
            <TypographyText variant={ "body2" }>
                By creating an Account on our Service, you agree to subscribe to newsletters, marketing or promotional
                materials and other information we may send. However, you may opt out of receiving any, or all, of these
                communications from us by following the unsubscribe link or by emailing at.
            </TypographyText>

            <TypographyH2 variant={ "h2" }>3. Purchases</TypographyH2>
            <TypographyText variant={ "body2" }>
                If you wish to purchase any product or service made available through Service (<b>“Purchase”</b>), you
                may be asked to supply certain information relevant to your Purchase including, without limitation, your
                credit card number, the expiration date of your credit card, your billing address.
                <br/><br/>You represent and warrant that: (i) you have the legal right to use any credit card(s) or
                other payment method(s) in connection with any Purchase; and that (ii) the information you supply to us
                is true, correct and complete.
                <br/><br/>We may employ the use of third party services for the purpose of facilitating payment and the
                completion of Purchases. By submitting your information, you grant us the right to provide the
                information to these third parties subject to our Privacy Policy.
                <br/><br/>We reserve the right to refuse or cancel your order at any time for reasons including but not
                limited to: product or service availability, errors in the description or price of the product or
                service, error in your order or other reasons.
                <br/><br/>We reserve the right to refuse or cancel your order if fraud or an unauthorized or illegal
                transaction is suspected.
            </TypographyText>

            <TypographyH2 variant={ "h2" }>4. Contests, Sweepstakes and Promotions</TypographyH2>
            <TypographyText variant={ "body2" }>
                Any contests, sweepstakes or other promotions (collectively, “Promotions”) made available through
                Service may be governed by rules that are separate from these Terms of Service. If you participate in
                any Promotions, please review the applicable rules as well as our Privacy Policy. If the rules for a
                Promotion conflict with these Terms of Service, Promotion rules will apply.
            </TypographyText>

            <TypographyH2 variant={ "h2" }>5. Additional fees</TypographyH2>
            <TypographyText variant={ "body2" }>
                The single price of our service includes only the development of your designs. Any other request such as
                API implementation, work with a specific style guide (existing components), animation, design, use of a
                programming language other than React, or any other request will cost an additional price. The
                DevOnDemand team will contact you by email, and will make a quote.
                If you have already placed an order but do not accept the quote for the additional work, we will not
                fulfill these additional requests.
                If you accept the quote and pay, these additional requests will affect the initial delivery time. A new
                delivery time will be sent to you by email.
            </TypographyText>

            <TypographyH2 variant={ "h2" }>6. Fee Changes</TypographyH2>
            <TypographyText variant={ "body2" }>
                DevOnDemand, at its sole discretion and at any time, may change the prices of its services. These
                changes will not impact orders already made.
            </TypographyText>

            <TypographyH2 variant={ "h2" }>7. Refunds</TypographyH2>
            <TypographyText variant={ "body2" }>
                No refunds will be issued.
            </TypographyText>

            <TypographyH2 variant={ "h2" }>8. Refunds</TypographyH2>
            <TypographyText variant={ "body2" }>
                No refunds will be issued.
            </TypographyText>

            <TypographyH2 variant={ "h2" }>9. Prohibited Uses</TypographyH2>
            <TypographyText variant={ "body2" }>
                You may use Service only for lawful purposes and in accordance with Terms. You agree not to use Service:
                <ol>
                    <li>In any way that violates any applicable national or international law or regulation.</li>
                    <li>For the purpose of exploiting, harming, or attempting to exploit or harm minors in any way by
                        exposing them to inappropriate content or otherwise.
                    </li>
                    <li>To transmit, or procure the sending of, any advertising or promotional material, including any
                        “junk mail”, “chain letter,” “spam,” or any other similar solicitation.
                    </li>
                    <li>To impersonate or attempt to impersonate Company, a Company employee, another user, or any other
                        person or entity.
                    </li>
                    <li>In any way that infringes upon the rights of others, or in any way is illegal, threatening,
                        fraudulent, or harmful, or in connection with any unlawful, illegal, fraudulent, or harmful
                        purpose or activity.
                    </li>
                    <li>To engage in any other conduct that restricts or inhibits anyone’s use or enjoyment of Service,
                        or which, as determined by us, may harm or offend Company or users of Service or expose them to
                        liability.
                    </li>
                </ol>
                <br/><br/>Additionally, you agree not to:
                <ol>
                    <li>Use Service in any manner that could disable, overburden, damage, or impair Service or interfere
                        with any other party’s use of Service, including their ability to engage in real time activities
                        through Service.
                    </li>
                    <li>Use any robot, spider, or other automatic device, process, or means to access Service for any
                        purpose, including monitoring or copying any of the material on Service.
                    </li>
                    <li>Use any manual process to monitor or copy any of the material on Service or for any other
                        unauthorized purpose without our prior written consent.
                    </li>
                    <li>Use any device, software, or routine that interferes with the proper working of Service.</li>
                    <li>Introduce any viruses, trojan horses, worms, logic bombs, or other material which is malicious
                        or technologically harmful.
                    </li>
                    <li>Attempt to gain unauthorized access to, interfere with, damage, or disrupt any parts of Service,
                        the server on which Service is stored, or any server, computer, or database connected to
                        Service.
                    </li>
                    <li>Attack Service via a denial-of-service attack or a distributed denial-of-service attack.</li>
                    <li>Take any action that may damage or falsify Company rating.</li>
                    <li>Otherwise attempt to interfere with the proper working of Service.</li>
                </ol>

            </TypographyText>


            <TypographyH2 variant={ "h2" }>10. No Use By Minors</TypographyH2>
            <TypographyText>Service is intended only for access and use by individuals at least eighteen (18) years old.
                By accessing or using any of Company, you warrant and represent that you are at least eighteen (18)
                years of age and with the full authority, right, and capacity to enter into this agreement and abide by
                all of the terms and conditions of Terms. If you are not at least eighteen (18) years old, you are
                prohibited from both the access and usage of Service</TypographyText>
            <TypographyH2 variant={ "h2" }>11. Intellectual Property</TypographyH2>
            <TypographyText>The code, features and functionality created for our clients are and will remain the
                exclusive property of our clients.</TypographyText>
            <TypographyH2 variant={ "h2" }>12. Copyright Policy</TypographyH2>
            <TypographyText>
                Our customers must ensure that the service they develop with our platform respects
                intellectual property rights and does not violate any law.
                <br/><br/>Our customers are solely responsible for the content created.
                <br/><br/>If you are the copyright owner, or licensed on behalf of one of them, and you believe that the
                copyrighted work has been copied in a way that constitutes an infringement of the right copyright,
                please submit your complaint by email to devondemandco@gmail.com, with the subject line: "Copyright
                Infringement" and include in your complaint a detailed description of the alleged infringement as
                detailed below. below, under "DMCA Notice and Procedure for Copyright Infringement Claims". We will then
                pass your request on to our concerned customers.
                <br/><br/>You may be held liable for damages (including costs and attorney fees) for misrepresentation
                or bad faith claims about the infringement of any content found on and / or through the service on your
                copyright.
            </TypographyText>

            <TypographyH2 variant={ "h2" }>14. DMCA Notice and Procedure for Copyright Infringement
                Claims</TypographyH2>
            <TypographyText>
                You may submit a notification pursuant to the Digital Millennium Copyright Act (DMCA) by providing our
                Copyright Agent with the following information in writing (see 17 U.S.C 512(c)(3) for further detail):
                <ul>
                    <li>an electronic or physical signature of the person authorized to act on behalf of the owner of
                        the copyright's interest;
                    </li>
                    <li>a description of the copyrighted work that you claim has been infringed, including the URL
                        (i.e., web page address) of the location where the copyrighted work exists or a copy of the
                        copyrighted work;
                    </li>
                    <li>identification of the URL or other specific location on Service where the material that you
                        claim is infringing is located
                    </li>
                    <li>your address, telephone number, and email address;</li>
                    <li>a statement by you that you have a good faith belief that the disputed use is not authorized by
                        the copyright owner, its agent, or the law;
                    </li>
                    <li>a statement by you, made under penalty of perjury, that the above information in your notice is
                        accurate and that you are the copyright owner or authorized to act on the copyright owner's
                        behalf.
                    </li>
                </ul>
                You can contact our Copyright Agent via email at <a href="mailto:">devondemandco@gmail.com</a>
            </TypographyText>

            <TypographyH2 variant={ "h2" }>15. Error Reporting and Feedback</TypographyH2>
            <TypographyText>You may provide us directly at devondemandco@gmail.com with information and feedback
                concerning errors, suggestions for improvements, ideas, problems, complaints, and other matters related
                to our Service (“Feedback”). You acknowledge and agree that: (i) you shall not retain, acquire or assert
                any intellectual property right or other right, title or interest in or to the Feedback; (ii) Company
                may have development ideas similar to the Feedback; (iii) Feedback does not contain confidential
                information or proprietary information from you or any third party; and (iv) Company is not under any
                obligation of confidentiality with respect to the Feedback. In the event the transfer of the ownership
                to the Feedback is not possible due to applicable mandatory laws, you grant Company and its affiliates
                an exclusive, transferable, irrevocable, free-of-charge, sub-licensable, unlimited and perpetual right
                to use (including copy, modify, create derivative works, publish, distribute and commercialize) Feedback
                in any manner and for any purpose.</TypographyText>
            <TypographyH2 variant={ "h2" }>16. Links To Other Web Sites</TypographyH2>
            <TypographyText>
                Our Service may contain links to third party web sites or services that are not owned or controlled by
                BCL
                <br/><br/>BCL has no control over, and assumes no responsibility for the content, privacy policies, or
                practices of any third party web sites or services. We do not warrant the offerings of any of these
                entities/individuals or their websites.
                <br/><br/>YOU ACKNOWLEDGE AND AGREE THAT BCL SHALL NOT BE RESPONSIBLE OR LIABLE, DIRECTLY OR INDIRECTLY,
                FOR ANY DAMAGE OR LOSS CAUSED OR ALLEGED TO BE CAUSED BY OR IN CONNECTION WITH USE OF OR RELIANCE ON ANY
                SUCH CONTENT, GOODS OR SERVICES AVAILABLE ON OR THROUGH ANY SUCH THIRD PARTY WEB SITES OR SERVICES.
                <br/><br/>WE STRONGLY ADVISE YOU TO READ THE TERMS OF SERVICE AND PRIVACY POLICIES OF ANY THIRD PARTY
                WEB SITES OR SERVICES THAT YOU VISIT.
            </TypographyText>
            <TypographyH2 variant={ "h2" }>17. Disclaimer Of Warranty</TypographyH2>
            <TypographyText>
                THESE SERVICES ARE PROVIDED BY COMPANY ON AN “AS IS” AND “AS AVAILABLE” BASIS. COMPANY MAKES NO
                REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO THE OPERATION OF THEIR SERVICES, OR
                THE INFORMATION, CONTENT OR MATERIALS INCLUDED THEREIN. YOU EXPRESSLY AGREE THAT YOUR USE OF THESE
                SERVICES, THEIR CONTENT, AND ANY SERVICES OR ITEMS OBTAINED FROM US IS AT YOUR SOLE RISK.
                <br/><br/>NEITHER COMPANY NOR ANY PERSON ASSOCIATED WITH COMPANY MAKES ANY WARRANTY OR REPRESENTATION
                WITH RESPECT TO THE COMPLETENESS, SECURITY, RELIABILITY, QUALITY, ACCURACY, OR AVAILABILITY OF THE
                SERVICES. WITHOUT LIMITING THE FOREGOING, NEITHER COMPANY NOR ANYONE ASSOCIATED WITH COMPANY REPRESENTS
                OR WARRANTS THAT THE SERVICES, THEIR CONTENT, OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE SERVICES
                WILL BE ACCURATE, RELIABLE, ERROR-FREE, OR UNINTERRUPTED, THAT DEFECTS WILL BE CORRECTED, THAT THE
                SERVICES OR THE SERVER THAT MAKES IT AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS OR THAT
                THE SERVICES OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE SERVICES WILL OTHERWISE MEET YOUR NEEDS OR
                EXPECTATIONS.
                <br/><br/>COMPANY HEREBY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, STATUTORY, OR
                OTHERWISE, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, AND FITNESS
                FOR PARTICULAR PURPOSE.
                <br/><br/>THE FOREGOING DOES NOT AFFECT ANY WARRANTIES WHICH CANNOT BE EXCLUDED OR LIMITED UNDER
                APPLICABLE LAW.
            </TypographyText>
            <TypographyH2 variant={ "h2" }>18. Limitation Of Liability</TypographyH2>
            <TypographyText>EXCEPT AS PROHIBITED BY LAW, YOU WILL HOLD US AND OUR OFFICERS, DIRECTORS, EMPLOYEES, AND
                AGENTS HARMLESS FOR ANY INDIRECT, PUNITIVE, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGE, HOWEVER IT
                ARISES (INCLUDING ATTORNEYS' FEES AND ALL RELATED COSTS AND EXPENSES OF LITIGATION AND ARBITRATION, OR
                AT TRIAL OR ON APPEAL, IF ANY, WHETHER OR NOT LITIGATION OR ARBITRATION IS INSTITUTED), WHETHER IN AN
                ACTION OF CONTRACT, NEGLIGENCE, OR OTHER TORTIOUS ACTION, OR ARISING OUT OF OR IN CONNECTION WITH THIS
                AGREEMENT, INCLUDING WITHOUT LIMITATION ANY CLAIM FOR PERSONAL INJURY OR PROPERTY DAMAGE, ARISING FROM
                THIS AGREEMENT AND ANY VIOLATION BY YOU OF ANY FEDERAL, STATE, OR LOCAL LAWS, STATUTES, RULES, OR
                REGULATIONS, EVEN IF COMPANY HAS BEEN PREVIOUSLY ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. EXCEPT AS
                PROHIBITED BY LAW, IF THERE IS LIABILITY FOUND ON THE PART OF COMPANY, IT WILL BE LIMITED TO THE AMOUNT
                PAID FOR THE PRODUCTS AND/OR SERVICES, AND UNDER NO CIRCUMSTANCES WILL THERE BE CONSEQUENTIAL OR
                PUNITIVE DAMAGES. SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF PUNITIVE, INCIDENTAL OR
                CONSEQUENTIAL DAMAGES, SO THE PRIOR LIMITATION OR EXCLUSION MAY NOT APPLY TO YOU.</TypographyText>
            <TypographyH2 variant={ "h2" }> 19. Termination</TypographyH2>
            <TypographyText>
                We may terminate or suspend your account and bar access to Service immediately, without prior notice or
                liability, under our sole discretion, for any reason whatsoever and without limitation, including but
                not limited to a breach of Terms.
                <br/><br/>If you wish to terminate your account, you may simply discontinue using Service.
                <br/><br/>All provisions of Terms which by their nature should survive termination shall survive
                termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and
                limitations of liability.</TypographyText>
            <TypographyH2 variant={ "h2" }>20. Governing Law</TypographyH2>
            <TypographyText>
                These Terms shall be governed and construed in accordance with the laws of France without regard to its
                conflict of law provisions.
                <br/><br/>Our failure to enforce any right or provision of these Terms will not be considered a waiver
                of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the
                remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement
                between us regarding our Service and supersede and replace any prior agreements we might have had
                between us regarding Service.</TypographyText>

            <TypographyH2 variant={ "h2" }> 21. Changes To Service</TypographyH2>
            <TypographyText>We reserve the right to withdraw or amend our Service, and any service or material we
                provide via Service, in our sole discretion without notice. We will not be liable if for any reason all
                or any part of Service is unavailable at any time or for any period. From time to time, we may restrict
                access to some parts of Service, or the entire Service, to users, including registered
                users.</TypographyText>

            <TypographyH2 variant={ "h2" }>22. Amendments To Terms</TypographyH2>
            <TypographyText>
                We may amend Terms at any time by posting the amended terms on this site. It is your responsibility to
                review these Terms periodically.
                <br/><br/>Your continued use of the Platform following the posting of revised Terms means that you
                accept and agree to the changes. You are expected to check this page frequently so you are aware of any
                changes, as they are binding on you.
                <br/><br/>By continuing to access or use our Service after any revisions become effective, you agree to
                be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use
                Service.
            </TypographyText>
            <TypographyH2 variant={ "h2" }> 23. Waiver And Severability</TypographyH2>
            <TypographyText>
                No waiver by Company of any term or condition set forth in Terms shall be deemed a further or
                continuing waiver of such term or condition or a waiver of any other term or condition, and any
                failure of Company to assert a right or provision under Terms shall not constitute a waiver of
                such right or provision.
                <br/><br/>If any provision of Terms is held by a court or other tribunal of competent
                jurisdiction to be invalid, illegal or unenforceable for any reason, such provision shall be
                eliminated or limited to the minimum extent such that the remaining provisions of Terms will
                continue in full force and effect.
            </TypographyText>
            <TypographyH2 variant={ "h2" }> 24. Acknowledgement</TypographyH2>
            <TypographyText>BY USING SERVICE OR OTHER SERVICES PROVIDED BY US, YOU ACKNOWLEDGE THAT YOU HAVE
                READ THESE TERMS OF SERVICE AND AGREE TO BE BOUND BY THEM.</TypographyText>
            <TypographyH2 variant={ "h2" }> 25. Contact Us</TypographyH2>
            <TypographyText>
                Please send your feedback, comments, requests for technical support:
                <br/>By email: <a href="mailto:devondemandco@gmail.com">devondemandco@gmail.com</a>
            </TypographyText>
        </ContainerBox>
    )
}

const ContainerBox = styled(Container)`
  margin: 70px auto;
`;
const TypographyH1 = styled(Typography)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 24px;
    line-height: 1.3em;
    letter-spacing: 0.03em;
    @media (max-width: 768px) {
        font-size: 16px;
    }
`;
const TypographyDate = styled(Typography)`
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    line-height: 1.3em;
    margin: 20px 0;
    letter-spacing: 0.03em;
    @media (max-width: 768px) {
        font-size: 10px;
    }
`;
const TypographyH2 = styled(Typography)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 18px;
    line-height: 1.3em;
    letter-spacing: 0.03em;
    margin:20px 0  10px;
    @media (max-width: 768px) {
        font-size: 10px;
    }
`;
const TypographyText = styled(Typography)`
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 16px;
    line-height: 1.5em;
    letter-spacing: 0.03em;
    & a{
        color: #000;
    }
    & b{
        font-family: "GT Walsheim Pro Bold", sans-serif;
    }
    & ol{
      margin-left: 40px;
      list-style-type: decimal;
    }
    @media (max-width: 768px) {
        font-size: 10px;
    }
`;