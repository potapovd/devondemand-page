import React, { Component } from "react"
import "aos/dist/aos.css"
import AOS from "aos"
import styled from "styled-components"
import { Box, Container, Grid, Typography } from "@material-ui/core"

import Accordion from "../../accordion/index"
import { Element, Link } from "react-scroll"

import database from "../../../database/database.json"
import Header from "../../typography/header"

export default class FaqPage extends Component {

    componentDidMount() {
        AOS.init({duration: 700})
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }

    renderSidebar = () => {
        const {faqs} = database;

        return faqs.map((item, index) => {
            return (
                <li key={ item.theme }>
                    <LinkLeftStick activeClass="active"
                                   to={ `item${ index }` }
                                   spy={ true }
                                   smooth={ true } duration={ 500 }
                    >{ item.theme }</LinkLeftStick>
                </li>
            )
        })
    }

    renderQuestions = () => {
        const {faqs} = database;

        return faqs.map((item, index) => {
            return (
                <Element name={ `item${ index }` } key={ item.theme }>
                    <TypographyListTitle>
                        { `${ index + 1 }. ${ item.theme }` }
                    </TypographyListTitle>
                    <BoxListAccordion>
                        <Accordion questions={ item.questions }/>
                    </BoxListAccordion>
                </Element>
            )
        })
    }


    render() {
        return (
            <ContainerFaq>
                <Grid container>
                    <Grid item md={ 2 }>
                        <NavDesktop>
                            <ul>
                                { this.renderSidebar() }
                            </ul>
                        </NavDesktop>
                    </Grid>
                    <Grid item xs={ 12 } md={ 8 }>
                        <Header text='Questions? Look here.'/>
                        <TypographyLinkMail>
                            Can’t find an answer? Email &nbsp;
                            <a href="mailto:devondemandco@gmail.com">
                                devondemandco@gmail.com
                            </a>
                        </TypographyLinkMail>
                        <Box>
                            { this.renderQuestions() }
                        </Box>
                    </Grid>
                </Grid>

            </ContainerFaq>
        )
    }
}

const ContainerFaq = styled(Container)``;
const TypographyLinkMail = styled(Typography)`
    font-family: "GT Walsheim Pro Medium", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    color: #919eab;
    opacity: 0.8;
    margin: 40px auto 70px;
    & a{
      color: #919eab;
      opacity: 0.8;
      text-decoration: none;
      &:hover{
        text-decoration: underline;
      }
    }
`;
const TypographyListTitle = styled(Typography)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    text-align: left;
    color: #000;
    margin:20px 0; 
`;
const BoxListAccordion = styled(Box)``;
const NavDesktop = styled.nav`
    position: sticky;
    top: 0px;
    & > ul{
      list-style: none;
      padding-left: 0;
      margin-left: 0;
    }
    @media (max-width: 960px) {
        display: none;
    }
`;

const LinkLeftStick = styled(Link)`
    font-family: "GT Walsheim Pro Medium", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    text-align: left;
    color: #919eab;
    opacity: 0.8;
    margin: 10px 0;
    display: block;
     &.active{
      color:#000;
    }
`

