import React from "react"
import styled from "styled-components"

import { Container } from "@material-ui/core"

import ProcessHeader from "../process/header"
import ProcessTest from "../process/containerTest"

import enhance from "../../../video/portfolio-enhance2.mp4"
import lockerapp from "../../../video/portfolio-lockerapp.mp4"
import speak2leads from "../../../video/portfolio-speak2leads.mp4"
import univertop from "../../../video/portfolio-univertop.mp4"
import videoPoster from "../../../video/video-poster.png"

export default function PortfolioPage() {
    return (
        <ContainerProcess>
            <ProcessHeader
                header='This is our working process.'
                subheader='Attract talent with beautiful job posts and manage candidates in an easy-to-use and powerful tool'
            />

            <ProcessTest name={ "enhance" } score={ "98/100 Speed Test" } poster={ videoPoster } video={ enhance }
                         url={ "#" }/>
            <ProcessTest name={ "speak2leads" } score={ "97/100 Speed Test" } poster={ videoPoster }
                         video={ speak2leads } url={ "#" }/>
            <ProcessTest name={ "univertop" } score={ "99/100 Speed Test" } poster={ videoPoster } video={ univertop }
                         url={ "#" }/>
            <ProcessTest name={ "lockerapp" } score={ "98/100 Speed Test" } poster={ videoPoster } video={ lockerapp }
                         url={ "#" }/>

        </ContainerProcess>
    )
}
const ContainerProcess = styled(Container)`
    margin-bottom: 120px;
    @media (max-width: 960px) {
        margin-bottom: 70px;
    }
`;
