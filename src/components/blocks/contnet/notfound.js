import React from "react"
import { Container, Typography } from "@material-ui/core"

const NotFoundPage = () => (
    <Container>
        <Typography variant={ "h1" }>NOT FOUND</Typography>
        <Typography variant={ "body1" }>You just hit a route that doesn&#39;t exist... the sadness.</Typography>
    </Container>
)

export default NotFoundPage