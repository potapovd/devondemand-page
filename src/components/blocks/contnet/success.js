import React, { Component } from "react"
import "aos/dist/aos.css"
import styled from "styled-components"
import { Link } from "gatsby";
import { Avatar, Box, Container, Typography } from "@material-ui/core"

import Header from "../../typography/headerCenter";
import SubheaderCenter from "../../typography/subheaderCenter";
import Button from "../../buttons/start"
import SuccessForm from "../../forms/successPage"

import logo from "../../../images/logo_dod.svg"
import smile from "../../../images/successGif.gif"


//export default class SuccessPage extends Component {
export default class SuccessPage extends Component {

    // componentDidMount() {
    //     AOS.init({duration: 700})
    // }
    //
    // componentWillReceiveProps() {
    //     AOS.refresh();
    // }


    render() {
        const formData = {
            option: "",
            link: "",
            comment: "",
            // pages: "",
            // mail: "",
            // api: "",
        }
        return (
            <ContainerSuccess>
                <BoxTopMenu>
                    <Link to='/'>
                        <AvatarLogo src={ logo } variant='square'/>
                    </Link>
                    <LinkMenuButton to='/'>
                        <Button text='Make another order'/>
                    </LinkMenuButton>
                </BoxTopMenu>

                <BoxFormContainer>
                    <AvatarSmile src={ smile } variant='square'/>
                    <Header text='Thank you for your order!'/>
                    <SubheaderCenter text='Before to start, we have a few questions'/>
                    <SuccessForm formData={ formData }/>
                </BoxFormContainer>

                <BoxFooter>
                    <LinkCustomerServ to='/'>
                        Customer Service
                    </LinkCustomerServ>
                    <TypographyCopy>
                        &copy; 2020, DEVONDEMAND.CO. All Rights Reserved.
                    </TypographyCopy>
                </BoxFooter>
            </ContainerSuccess>
        )
    }
}

const ContainerSuccess = styled(Container)``;

//HEADER
const BoxTopMenu = styled(Box)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 40px 0;
`;
const LinkMenuButton = styled(Link)`
    text-decoration: none;
    &:hover{
        text-decoration: none;
    }
`;
const AvatarLogo = styled(Avatar)`
    width: 135px;
    height: auto;
    position: relative;
    @media (max-width: 960px) {
        width: 88px
    }
`;
// CONTENT
const BoxFormContainer = styled(Box)`
  width: 100%;
  max-width: 660px;
  margin: 0 auto;
  text-align: center;
`;

const AvatarSmile = styled(Avatar)`
  width: 141px;
  height: 141px;
  margin: 0 auto;
`;

//FOOTER
const BoxFooter = styled(Box)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 40px 0;
  @media (max-width: 960px) {
      flex-direction: column;
  }
`;


const TypographyCopy = styled(Typography)`
    font-family: "GT Walsheim Pro Bold", sans-serif;
    font-size: 10px;
    text-align: left;
    color: #919eab;
    @media (max-width: 960px) {
      text-align: center;
    }
 `;
const LinkCustomerServ = styled(Link)`
    font-family: "GT Walsheim Pro Bold", sans-serif;
    text-decoration: underline;
    font-size: 20px;
    letter-spacing: 0.03em;
    line-height: 53px;
    text-align: center;
    color: #919eab;
    transition: all .5s;
    &:hover{
      text-decoration: none;
    }
    @media (max-width: 960px) {
      font-size: 12px;
      text-align: center;
    }
`
// const LinkCustomerServ = styled(Link)` `;
// const LinkCustomerServ = styled(Link)` `;





