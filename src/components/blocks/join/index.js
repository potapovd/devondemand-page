import React, { Component } from "react"
import "aos/dist/aos.css"
import AOS from "aos"
import styled from "styled-components"

import { Avatar, Box, Container, Grid } from "@material-ui/core"
import { Link as LinkTo } from "react-scroll"

import Header from "../../typography/header"
import SubHeader from "../../typography/subheaderLeft"
import Button from "../../buttons/start"
//import Comment from "../../comments/index"
import Comment from "../../comments/index"

import img from "./img/man.jpg"

export default class Join extends Component {

    componentDidMount() {
        AOS.init({duration: 700})
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }

    render() {
        return (
            <Box>
                <Container>
                    <Header align='center'
                            text="Join the club."/>
                    <SubHeader align='center' space='sm'
                               text="Hundreds of leading creative companies are working with us to develop their designs."/>
                    <Box>
                        <LinkTo
                            to="formSection"
                            spy={ true }
                            smooth={ true }
                            offset={ -70 }
                            duration={ 500 }
                        >
                            <Button text='Start now'/>
                        </LinkTo>
                    </Box>
                </Container>
                <ContainerComments disableGutters>
                    <Grid container spacing={ 1 }>
                        <GridLeft item xs={ 4 }>
                            <BoxReviews data-aos='fade-right' data-delay='300'>
                                <Comment isClosed={ true } name='Sammy Owen'
                                         text='Hiring remote makes it more important than ever to work together as a team to attract, review and hire the best candidates.'/>
                                <Comment isClosed={ true } name='Joe'
                                         text='Lorem Ipsum 1 Hiring remote makes it more important than ever to work together as a team to attract'/>
                                <Comment isClosed={ true } name='Joe'
                                         text='Lorem Ipsum 2 Hiring remote makes it more important than ever to work together as a team to attract'/>
                                <Comment isClosed={ true } name='Joe'
                                         text='Lorem Ipsum 3 Hiring remote makes it more important than ever to work together as a team to attract'/>
                            </BoxReviews>
                        </GridLeft>
                        <GridRight item xs={ 8 } data-aos='fade-left'>
                            <AvatarWoman variant='rounded' src={ img } alt=''/>
                        </GridRight>
                    </Grid>
                </ContainerComments>
            </Box>
        )
    }
}

const GridRight = styled(Grid)`
    display:flex;
    justify-content: flex-end;
`;
const ContainerComments = styled(Container)`
  margin-top: 40px;
  margin-bottom: 100px;
`;
const GridLeft = styled(Grid)``;
const BoxReviews = styled(Box)`
    position: relative;
    height: 100%;
    z-index:1;
    & .Flipcard-front{
      max-width: 350px;
      overflow: visible;
        @media (max-width: 767px) {
             max-width: 115px;
        }
    }
    & .Flipcard-back{
      width: 350px;
      overflow: visible;
    }
    & > div{
        position: absolute;
    }
    & > div:nth-child(1){
        top:10px;
        right:-120%;
        @media (max-width: 767px) {
            top: 10px; 
             //right:auto;
             //left:20px;
        }  
        //z-index:4;
    }
    & > div:nth-child(2){
        top:20%;
        left:5%;
        @media (max-width: 767px) {
             //right:auto;
             //left:200px;
        } 
        //z-index:3;
        & > div > div > div{
            //width:350px !important;
        }
    }
    & > div:nth-child(3){
        top:45%;
        right:-99%;
        @media (max-width: 767px) {
             //right:auto;
             //left:20px;
        } 
        //z-index:2;
    }
    & > div:nth-child(4){
        bottom:20%;
        left:15%;
        @media (max-width: 767px) {
             //right:auto;
             //left:20px;
        } 
        //z-index:1;
        & > div > div > div{
            //width:350px !important;
        }
    }
`;
const AvatarWoman = styled(Avatar)`
    max-width:721px;
    height:auto;
    width:100%;
    position: relative;
    right:-15%;
`;