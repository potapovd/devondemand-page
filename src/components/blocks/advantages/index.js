import React, { Component } from "react"
import "aos/dist/aos.css"
import AOS from "aos"
import styled from "styled-components"
import { Link } from "gatsby"

import { Box, Container, Grid, Typography } from "@material-ui/core"

import Header from "../../typography/headerRight"
import Subheader from "../../typography/subheaderRight"
import Button from "../../buttons/start"
import StartWhite from "../../buttons/startWhite"


export default class Advantages extends Component {

    componentDidMount() {
        AOS.init({duration: 700})
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }

    render() {
        return (
            <Box>
                <ContainerBlue maxWidth={ false } disableGutters>
                    <Container>
                        <GridOut container spacing={ 10 }>
                            <GridFlexLeft item xs={ 12 } sm={ 12 } md={ 5 }>
                                <TypographyAdv data-aos='fade-down' variant="h6">Bring data from any source that are
                                    always
                                    up-to-date</TypographyAdv>
                                <TypographyAdv data-aos='fade-down' variant="h6">Generate billions contents at the
                                    moment of
                                    engagement</TypographyAdv>
                                <TypographyAdv data-aos='fade-down' variant="h6">Optimize in real-time your content
                                    performance</TypographyAdv>
                            </GridFlexLeft>
                            <GridFlexRight item xs={ 12 } sm={ 12 } md={ 7 } data-aos='fade-left'>
                                <Header align='end' text="Generate powerful content, reap more than conversion"/>
                                <Subheader align='end'
                                           text="From boring content campaign to unexpected one, there is us. Break your email vision &amp; give your content something your subscribers have never seen before."/>
                                <BoxResp align='end'>
                                    <LinkMenuButton to='/'>
                                        <Button text='Start now'/>
                                    </LinkMenuButton>
                                </BoxResp>
                            </GridFlexRight>
                        </GridOut>
                    </Container>
                </ContainerBlue>
                <BoxDarkBlue data-aos='zoom-in'>
                    <BoxDarkBlueHeder variant="h5" data-aos='fade-down' color="initial">
                        Try dev on demand today!
                    </BoxDarkBlueHeder>
                    <Box align='center'>
                        <LinkMenuButton to='/'>
                            <StartWhite text='Start now'/>
                        </LinkMenuButton>
                    </Box>
                </BoxDarkBlue>
            </Box>
        )
    }

}

const ContainerBlue = styled(Container)`
    background-color: #E0EAF5;
    padding:100px 0;
`;

const BoxResp = styled(Box)`
  @media (max-width: 960px) {
        text-align: center;
    }
`;

const TypographyAdv = styled(Typography)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    line-height: 1.3em;
    width: 100%;
    text-align: left;
    @media (max-width: 960px) {
      text-align: center;
       margin-bottom:20px;
    }
`;
const GridOut = styled(Grid)`
    display: flex;
`;
const GridFlexLeft = styled(Grid)`
    display: flex;
    flex-direction: column;
    justify-content: space-between ;
    @media (max-width: 960px) {
        order: 2;
    }
    @media (max-width: 767px) {
        order: 2;
    }  
`;

const GridFlexRight = styled(Grid)`
    display: flex;
    flex-direction: column;
    @media (max-width: 960px) {
        order: 1;
    }
    @media (max-width: 767px) {
        order: 1;
    }

`;

const BoxDarkBlue = styled(Box)`
    max-width: 830px;
    margin: 0 auto;
    position: relative;
    top: -40px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background: #4b6fff;
    border-radius: 10px;
    padding: 80px 0;
    @media (max-width: 830px) {
        margin: 0 20px;
        padding: 40px 0;
    }
`;
const BoxDarkBlueHeder = styled(Typography)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 40px;
    letter-spacing: 0.03em;
    text-align: center;
    color: #fff;
    margin-bottom: 20px;
    @media (max-width: 960px) {
        font-size: 30px;
    }
    @media (max-width: 767px) {
        font-size: 20px;
    }
`;

const LinkMenuButton = styled(Link)`
    text-decoration: none;
    &:hover{
        text-decoration: none;
    }
`;