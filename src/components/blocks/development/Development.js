import React, { Component } from "react"
import "aos/dist/aos.css"
import AOS from "aos"
import styled from "styled-components"

import { Box, Container, Grid, List, ListItem, ListItemIcon, SvgIcon, Typography } from "@material-ui/core"

import Header from "../../typography/header"
import SubHeaderTop from "../../typography/subheaderTop"


export default class DevelopmentAndForm extends Component {

    componentDidMount() {
        AOS.init({duration: 700})
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }

    render() {
        return (
            <Box>
                <ContainerDevelopment>
                    <BoxColumns>
                        <Grid container spacing={ 3 }>
                            <GridHeader item sm={ 12 } md={ 6 }>
                                <Header align='left' text="Front end development on demand"/>
                            </GridHeader>
                            <GridForm id='formSection' item sm={ 12 } md={ 6 }>
                                <SubHeaderTop align='left' space='md'
                                              text="Attract talent with beautiful job posts and manage candidates in an easy-to-use and powerful tool."/>
                                <List>
                                    <ListItemDev data-aos='fade-down' data-aos-delay='100'>
                                        <ListItemIcon>
                                            <SvgIcon viewBox='0 0 34 23'>
                                                <g>
                                                    <g>
                                                        <path fill="#919eab"
                                                              d="M4.042 5.873l9.78 9.38L29.725 0l4.036 3.864L13.834 23 0 9.755z"/>
                                                    </g>
                                                </g>
                                            </SvgIcon>
                                        </ListItemIcon>
                                        <ListItemTypography>Delivery in 3 business days</ListItemTypography>
                                    </ListItemDev>
                                    <ListItemDev data-aos='fade-down' data-aos-delay='200'>
                                        <ListItemIcon>
                                            <SvgIcon viewBox='0 0 34 23'>
                                                <g>
                                                    <g>
                                                        <path fill="#919eab"
                                                              d="M4.042 5.873l9.78 9.38L29.725 0l4.036 3.864L13.834 23 0 9.755z"/>
                                                    </g>
                                                </g>
                                            </SvgIcon>
                                        </ListItemIcon>
                                        <ListItemTypography>No contract</ListItemTypography>
                                    </ListItemDev>
                                </List>
                            </GridForm>
                        </Grid>
                    </BoxColumns>
                </ContainerDevelopment>
            </Box>
        )
    }


}

const ContainerDevelopment = styled(Container)``;

const ListItemDev = styled(ListItem)`
    @media (max-width: 960px) {
        padding-top: 4px;
        padding-bottom: 4px;
    }
`;
const ListItemTypography = styled(Typography)`
    font-family: "GT Walsheim Pro Bold", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    color: #919eab;
    @media (max-width: 960px) {
        font-size: 16px;
    }
    @media (max-width: 767px) {
        font-size: 12px;
    }
`;

const BoxColumns = styled(Box)``;
const GridHeader = styled(Grid)`
    display:flex;
    flex-direction: column;
    justify-content: center;
`;
const GridForm = styled(Grid)`
    display:flex;
    flex-direction: column;
    justify-content: center;
`;