import React from "react"
import { Link } from "gatsby"
import styled from "styled-components"

import { Avatar, Box, Container, Grid, Paper, Typography } from "@material-ui/core"
import { Link as LinkTo } from "react-scroll"

import HeaderCenter from "../../typography/headerCenter"
import SubHeaderCenter from "../../typography/subheaderCenter"
import Button from "../../buttons/start"

import adobe from "./img/adobe.svg"
import figma from "./img/figma.svg"
import sketch from "./img/sketch.svg"


export default function Extensions() {
    return (
        <ContainerExtensions id='extensionsSection'>
            <HeaderCenter align='center' text="We developed extensions, so you can share your design easily "/>
            <SubHeaderCenter align='center' space='sm'
                             text="Attract talent with beautiful job posts and manage candidates in an easy-to-use and powerful tool."/>
            <Box alignContent='center' align='center'>
                <LinkTo
                    to="wrapper_full_form"
                    spy={ true }
                    smooth={ true }
                    offset={ -70 }
                    duration={ 500 }
                >
                    <Button text='Start now'/>
                </LinkTo>
            </Box>
            <GridExt container spacing={ 5 }>
                <Grid item xs={ 4 }>
                    <PaperExt>
                        <TypographyExt variant="h6">Adobe XD</TypographyExt>
                        <LinkExt to='http://google.com'>See the extension</LinkExt>
                        <AvatatExt src={ adobe } alt="Adobe XD" variant='square'/>
                    </PaperExt>
                </Grid>
                <Grid item xs={ 4 }>
                    <PaperExt>
                        <TypographyExt variant="h6">Sketch</TypographyExt>
                        <LinkExt to='http://google.com'>See the extension</LinkExt>
                        <AvatatExt src={ sketch } alt="Sketch" variant='square'/>
                    </PaperExt>
                </Grid>
                <Grid item xs={ 4 }>
                    <PaperExt>
                        <TypographyExt variant="h6">Figma</TypographyExt>
                        <LinkExt to='http://google.com'>See the extension</LinkExt>
                        <AvatatExt src={ figma } alt="Figma" variant='square'/>
                    </PaperExt>
                </Grid>
            </GridExt>
        </ContainerExtensions>
    )
}

const ContainerExtensions = styled(Container)`
    margin-bottom: 70px;
`;

const GridExt = styled(Grid)`
    margin-top:70px;
    & > div:last-child img{
        max-height: 93px;
        width: auto;
    }
    @media (max-width: 960px) {
        margin-top: 50px;
        & > div:last-child img{
            max-height: 50px;
            width: auto;
        }
    }
    @media (max-width: 767px) {
        margin-top: 20px;
            & > div:last-child img{
            max-height: 30px;
            width: auto;
        }
    }
`;
const AvatatExt = styled(Avatar)`
    width: 100%;
    max-width: 93px;
    height: auto;
    margin: 0 auto;
    @media (max-width: 960px) {
        max-width: 50px;
    }
    @media (max-width: 767px) {
        max-width: 30px;
    }
`;
const TypographyExt = styled(Typography)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 40px;
    letter-spacing: 0.03em;
    text-align: center;
    color: #000;
    @media (max-width: 960px) {
        font-size: 20px;
    }
    @media (max-width: 767px) {
        font-size: 14px;
    }
`;
const LinkExt = styled(Link)`
    font-family: "GT Walsheim Pro Medium", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    text-align: center;
    color: #919eab;
    text-decoration: none;
    opacity: 0.71;
    margin:30px auto 90px ;
    &:hover{
        color: #029fff;
        text-decoration: underline;
    }
    @media (max-width: 960px) {
        font-size: 14px;
        margin:20px auto 50px ;
    }
    @media (max-width: 767px) {
        font-size: 8px;
        margin:10px auto 20px ;
    }
`;

const PaperExt = styled(Paper)`
    display: flex;
    flex-direction:column;
    border-radius: 40px;
    background: #fff;
    box-shadow: none;
    padding: 80px 0;
    @media (max-width: 960px) {
        padding: 40px 0;
        border-radius: 20px;
    }
    @media (max-width: 767px) {
        padding: 20px 0;
        border-radius: 10px;
    }
`;