import React, { Component } from "react"
import "aos/dist/aos.css"
import AOS from "aos"
import styled from "styled-components"

import { Avatar, Box, Container } from "@material-ui/core"
import { Link as LinkTo } from "react-scroll"

import Header from "../../typography/headerCenter"
import Subheader from "../../typography/subheaderCenter"
import Button from "../../buttons/start"

import img from "./img/integrate.svg"


export default class Integrate extends Component {

    componentDidMount() {
        AOS.init({duration: 700})
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }

    render() {
        return (
            <ContainerIntegrate>
                <Header align='center' text="We will integrate any tool you love"/>
                <Subheader align='center'
                           text="Attract talent with beautiful job posts and manage candidates  in an easy-to-use and powerful tool."/>
                <BoxButton alignContent='center' align='center'>
                    <LinkTo
                        to="formSection"
                        spy={ true }
                        smooth={ true }
                        offset={ -70 }
                        duration={ 500 }
                    >
                        <Button text='Start now'/>
                    </LinkTo>
                </BoxButton>
                <IntegrateImg data-aos='fade-up' variant='square' src={ img }/>
            </ContainerIntegrate>
        )
    }


}
const ContainerIntegrate = styled(Container)`
    margin: 70px auto 100px;
    @media (max-width: 960px) {
        margin: 70px auto 70px;
    }
`;
const IntegrateImg = styled(Avatar)`
    width: 100%;
    height: auto;
`;
const BoxButton = styled(Box)`
    margin: 20px 0;
`;
