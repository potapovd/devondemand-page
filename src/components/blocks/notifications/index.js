import React, { Component } from "react"
import "aos/dist/aos.css"
import AOS from "aos"
import styled from "styled-components"

import { Avatar, Box, Container, Grid } from "@material-ui/core"
import { Link as LinkTo } from "react-scroll"

import Header from "../../typography/header"
import SubHeader from "../../typography/subheaderLeft"
import Button from "../../buttons/start"

import img from "./img/notif.svg"

class Notifications extends Component {
    componentDidMount() {
        AOS.init({duration: 700})
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }

    render() {
        return (
            <ContainerNotifications>
                <Grid container spacing={ 1 }>
                    <Grid item xs={ 12 } sm={ 12 } md={ 6 }>
                        <Header
                            align='left' text="Get notified at every single steps."/>
                        <SubHeader
                            align='left'
                            text="Attract talent with beautiful job posts and manage candidates  in an easy-to-use and powerful tool."/>
                        <Box>
                            <LinkTo
                                to="formSection"
                                spy={ true }
                                smooth={ true }
                                offset={ -70 }
                                duration={ 500 }
                            >
                                <Button text='Start now'/>
                            </LinkTo>
                        </Box>
                    </Grid>
                    <Grid item xs={ 12 } sm={ 12 } md={ 6 }>
                        <NotifImg variant='square' src={ img } data-aos='fade-left'/>
                    </Grid>
                </Grid>
            </ContainerNotifications>
        )
    }


}

export default Notifications;

const ContainerNotifications = styled(Container)`
    margin: 140px auto;
`;

const NotifImg = styled(Avatar)`
    width: 100%;
    max-width: 650px;
    height: auto;
    position: relative;
`;