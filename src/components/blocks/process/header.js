import React, { Component } from "react"
import "aos/dist/aos.css"
import AOS from "aos"
import { Box, Container } from "@material-ui/core"
import { Link as LinkTo } from "react-scroll"
import Button from "../../buttons/start"

import HeaderCenter from "../../typography/headerCenter"
import SubHeaderCenter from "../../typography/subheaderCenter"

export default class ProcessHeader extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        AOS.init({duration: 700})
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }

    render() {
        return (
            <Container data-aos='fade-down'>
                <Box>
                    <HeaderCenter align='center' text={ this.props.header }/>
                    <SubHeaderCenter align='center' space='sm' text={ this.props.subheader }/>
                </Box>
                <Box alignContent='center' align='center'>
                    <LinkTo
                        to="formSection"
                        spy={ true }
                        smooth={ true }
                        offset={ -70 }
                        duration={ 500 }
                    >
                        <Button text='Start now'/>
                    </LinkTo>
                </Box>
            </Container>
        )
    }
}


