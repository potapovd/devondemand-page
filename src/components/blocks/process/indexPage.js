import React from "react"
import "aos/dist/aos.css"
import styled from "styled-components"

import { Container } from "@material-ui/core"

import ProcessHeader from "../process/header"
import ProcessVideo from "../process/containerVideo"

import videoSrc from "../../../video/index.mp4"
import videoPoster from "../../../video/video-poster.png"

export default function PortfolioPage() {
    return (
        <ContainerProcess id='processSection'>
            <ProcessHeader
                data-aos='fade-up'
                header='This is our working process.'
                subheader='Attract talent with beautiful job posts and manage candidates in an easy-to-use and powerful tool'
            />
            <ProcessVideo
                name={ "Watch the full video (2 min)" } poster={ videoPoster } video={ videoSrc } url={ "#" }/>
        </ContainerProcess>
    )
}
const ContainerProcess = styled(Container)`
    margin: 120px auto; 
    @media (max-width: 960px) {
        margin-bottom: 70px;
    }
`;
