import React, { Component } from "react"
import { Link } from "gatsby"
import "aos/dist/aos.css"
import AOS from "aos"
import styled from "styled-components"
import ModalVideo from "react-modal-video"
import { Avatar, Box, Typography } from "@material-ui/core"

class ProcessTest extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }
        this.openModal = this.openModal.bind(this)
    }

    componentDidMount() {
        AOS.init({duration: 700})
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }

    openModal() {
        this.setState({isOpen: true})
    }

    render() {
        return (
            <BoxProcess alignContent='center' align='center'>
                <VideoPreview
                    playsInline
                    muted
                    loop={ true }
                    autoPlay
                    data-aos='fade-in'
                    poster={ this.props.poster }
                >
                    <source type="video/mp4" src={ this.props.video }/>
                    <p>
                        Your browser doesn't support HTML5 video. Here is
                        a <a href={ this.props.video }>link to the video</a> instead.
                    </p>
                </VideoPreview>
                <LinkTest data-aos='fade-in' to={ this.props.url } onClick={ (e) => {
                    e.preventDefault()
                    this.setState({isOpen: !this.state.isOpen})
                } }>
                    { this.props.name }
                </LinkTest>

                <ModalVideo
                    channel='custom'
                    url={ this.props.video }
                    isOpen={ this.state.isOpen }
                    onClose={ () => this.setState({isOpen: false}) }/>
            </BoxProcess>
        )
    }
}

export default ProcessTest


const BoxProcess = styled(Box)`
    margin: 100px auto ;
    & .modal-video{
      background-color: rgba(0,0,0,0.8);
    }
    @media (max-width: 960px) {
        margin: 60px auto;
    }
`;
const VideoPreview = styled.video`
    display: block;
    width:100%;
    max-width: 720px;
    height:auto;
    border-radius: 20px;
`;
const LinkTest = styled(Link)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    text-align: center;
    color: #000;
    text-decoration: none;
    display: inline-block;
    padding: 20px;
    border-radius: 36px;
    background: #fff;
    box-shadow: 0 0 25px rgba(0, 0, 0, 0.14);
    position: relative;
    top: -30px;
    transition: all .3s;
    &:hover{
        background: #3854C3;
        color: #fff;
    }
    @media (max-width: 768px) {
        font-size: 11px;
    }
`;
const TypographyScore = styled(Typography)`
    font-family: "GT Walsheim Pro Bold", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    text-align: center;
    color: #919eab;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    @media (max-width: 768px) {
      position: relative;
      font-size: 11px;
      top: -20px;
    }
`;
const AvatarCheck = styled(Avatar)`
    display: inline-block;
    width: 34px;
    height: auto;
    margin-right: 20px;
    @media (max-width: 768px) {
        width: 22px;
    }
`;
