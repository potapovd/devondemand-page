import React, { Component } from "react"
import "aos/dist/aos.css"
import AOS from "aos"
import { Link } from "gatsby"
import styled from "styled-components"

import { Box, Container, Grid, List, ListItem, ListItemIcon, SvgIcon, Typography } from "@material-ui/core"

import Header from "../../typography/headerCenter"
import HeaderSm from "../../typography/headerSm"
import SubHeader from "../../typography/subheaderCenter"

class Price extends Component {

    componentDidMount() {
        AOS.init({duration: 700})
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }

    render() {
        return (
            <ContainerPrice id='priceSection'>
                <Header align='center' text="A simple pricing plan"/>
                <HeaderSm align='center' text="Fixed fee per page"/>
                <TypographyPrice data-aos='zoom-in' variant="h1" color="initial">
                    $350
                </TypographyPrice>
                <BoxList>
                    <Grid container spacing={ 1 }>
                        <ListPrice>
                            <Grid item xs={ 7 }>
                                <ListItemPrice data-aos='fade-right'>
                                    <ListItemIconSm>
                                        <SvgIcon viewBox='0 0 34 23'>
                                            <g>
                                                <g>
                                                    <path fill="#919eab"
                                                          d="M4.042 5.873l9.78 9.38L29.725 0l4.036 3.864L13.834 23 0 9.755z"/>
                                                </g>
                                            </g>
                                        </SvgIcon>
                                    </ListItemIconSm>
                                    <ListItemTypography>Delivery in 3 business days</ListItemTypography>
                                </ListItemPrice>
                            </Grid>
                            <Grid item xs={ 5 }>
                                <ListItemPrice data-aos='fade-left'>
                                    <ListItemIconSm>
                                        <SvgIcon viewBox='0 0 34 23'>
                                            <g>
                                                <g>
                                                    <path fill="#919eab"
                                                          d="M4.042 5.873l9.78 9.38L29.725 0l4.036 3.864L13.834 23 0 9.755z"/>
                                                </g>
                                            </g>
                                        </SvgIcon>
                                    </ListItemIconSm>
                                    <ListItemTypography>No contract</ListItemTypography>
                                </ListItemPrice>
                            </Grid>
                        </ListPrice>
                    </Grid>
                </BoxList>
                <SubHeader align='center' space='sm'
                           text="If you don’t have any active job posts you’ll still Have across to your career site and talent pool for no costs."/>
                <TypographyHeaderFaq data-aos='fade-down' variant="h5" component="h5">
                    <LinkFAQ to='faq'>Frequently Asked Questions</LinkFAQ>&nbsp;
                    around our pricing.
                </TypographyHeaderFaq>
            </ContainerPrice>
        )
    }

}

export default Price;

const ContainerPrice = styled(Container)`
    margin: 140px auto;
`;
const TypographyPrice = styled(Typography)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 140px;
    letter-spacing: 0.03em;
    text-align: center;
    color: #000;
    @media (max-width: 960px) {
        font-size: 100px;
    }
    @media (max-width: 767px) {
        font-size: 60px;
    }
`;

const BoxList = styled(Box)`
    max-width: 590px;
    margin: 0 auto;
`;
const ListPrice = styled(List)`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
`;
const ListItemTypography = styled(Typography)`
    font-family: "GT Walsheim Pro Bold", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    text-align: center;
    color: #919eab;
    @media (max-width: 960px) {
        font-size: 14px;
    }
    @media (max-width: 767px) {
        font-size: 12px;
    }
`;

const ListItemIconSm = styled(ListItemIcon)`
    min-width:30px;
`;
const ListItemPrice = styled(ListItem)`
    display: flex;
    align-items: center;
    justify-content: center;
    text-align:center;
`;

const TypographyHeaderFaq = styled(Typography)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 30px;
    letter-spacing: 0.03em;
    color: #000;
    text-align: center;
    margin: 40px auto;
    @media (max-width: 960px) {
        margin: 20px auto;
        font-size: 20px;
    }
    @media (max-width: 767px) {
        margin: 20px auto;
        font-size: 14px;
    }
`;
const LinkFAQ = styled(Link)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 30px;
    letter-spacing: 0.03em;
    color: #4B6FFF;
    text-align: center;
    text-decoration: underline;
    @media (max-width: 960px) {
        font-size: 20px;
    }
    @media (max-width: 767px) {
        font-size: 14px;
    }
`;