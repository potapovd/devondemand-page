import React, { Component } from "react";
import { Link } from "gatsby"
import styled from "styled-components"

import { Avatar, Box, Container, Drawer, Grid, IconButton, Link as LinkEx, Typography } from "@material-ui/core"
import { Link as LinkTo } from "react-scroll"

import logo from "../../../images/logo_dod.svg"
import MenuIcon from "@material-ui/icons/Menu"
import CloseIcon from "@material-ui/icons/Close"
import Button from "../../buttons/start"

import github from "../../blocks/footer/img/github.svg"
import twitter from "../../blocks/footer/img/twitter.svg"
import instagram from "../../blocks/footer/img/instagram.svg"

export default class Menu extends Component {
    state = {
        drawerIsOpen: false,
        overlayIsOpen: false
    }

    handleDrawerChange = () => {
        this.setState(prevState => ({drawerIsOpen: !prevState.drawerIsOpen}));
        this.setState(prevState => ({overlayIsOpen: !prevState.overlayIsOpen}));
    };


    render() {

        return (
            <ContainerMenu>
                <BoxOverlay style={ this.state.overlayIsOpen ? {} : {display: "none"} }/>
                <Grid container spacing={ 10 }>
                    <GridFlexCenter item xs={ 6 } sm={ 9 }>
                        <Link to='/'>
                            <AvatarLogo src={ logo } variant='square'/>
                        </Link>
                        <BoxMenuDesktop>

                            {/*<LinkMenuTo*/ }
                            {/*    to='extensionsSection'*/ }
                            {/*    spy={ true }*/ }
                            {/*    smooth={ true }*/ }
                            {/*    offset={ -70 }*/ }
                            {/*    duration={ 500 }*/ }
                            {/*>Extensions</LinkMenuTo>*/ }

                            <LinkMenu to='/portfolio'>Portfolio</LinkMenu>
                            <LinkMenu to='/faq'>FAQ</LinkMenu>
                        </BoxMenuDesktop>
                    </GridFlexCenter>
                    <GridFlexEnd item xs={ 6 } sm={ 3 }>
                        <BoxButtonTop>
                            <LinkMenuButton to='/'>
                                <Button text='Start now'/>
                            </LinkMenuButton>
                        </BoxButtonTop>
                        <BoxButtonMenuMobile>
                            <IconButtonToggler aria-label="close" size="medium" onClick={ this.handleDrawerChange }>
                                { this.state.drawerIsOpen ? (
                                    <CloseIcon fontSize="default"/>
                                ) : (
                                    <MenuIcon fontSize="default"/>
                                ) }
                            </IconButtonToggler>
                        </BoxButtonMenuMobile>
                    </GridFlexEnd>
                </Grid>
                <DrawerStyled
                    anchor='top'
                    variant="persistent"
                    open={ this.state.drawerIsOpen }
                >
                    <ContainerMobileTop>
                        <AvatarLogoMob src={ logo } variant='square'/>
                        <IconButtonTogglerMob aria-label="close" size="medium" onClick={ this.handleDrawerChange }>
                            <CloseIcon fontSize="default"/>
                        </IconButtonTogglerMob>
                    </ContainerMobileTop>
                    <ContainerMobile>
                        <LinkMenuMob to='/portfolio'>Portfolio</LinkMenuMob>
                        <LinkMenuMob to='/faq'>FAQ</LinkMenuMob>
                        <LinkButton to='/'>
                            <Button text='start now'/>
                        </LinkButton>
                        <LinkMenuMob to='/terms-of-service'>Terms of service</LinkMenuMob>
                        <LinkMenuMob to='/privacy-policy'>Privacy policy</LinkMenuMob>
                        <BoxSoc>
                            <LinkEx to='https://github.com/devondemandco'>
                                <AvatarSoc src={ github } variant='square'/>
                            </LinkEx>
                            <LinkEx to='https://www.instagram.com/devondemandco/'>
                                <AvatarSoc src={ instagram } variant='square'/>
                            </LinkEx>
                            <LinkEx to='https://twitter.com/devondemandco'>
                                <AvatarSoc src={ twitter } variant='square'/>
                            </LinkEx>
                        </BoxSoc>
                        <TypographyCopy>
                            &copy; 2020, DEVONDEMAND.CO. All Rights Reserved.
                        </TypographyCopy>
                    </ContainerMobile>
                </DrawerStyled>
            </ContainerMenu>
        )
    }
}

const BoxOverlay = styled(Box)`
  display: block;
  position: fixed;
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
  backdrop-filter: blur(3px);
  background-color:rgba(75, 111, 255, 0.6);
  z-index: 999;
  transition: all 1.5s;
`
const DrawerStyled = styled(Drawer)``
const LinkButton = styled(Link)`
  text-decoration: none;
`
const ContainerMenu = styled(Container)`
    margin: 40px auto 60px;
    @media (max-width: 960px) {
        margin: 20px auto 30px;
    }
`;
const AvatarLogo = styled(Avatar)`
    width: 135px;
    height: auto;
    position: relative;
    //position: absolute;
    //z-index: 99999;
    @media (max-width: 960px) {
        width: 88px
    }
`;
const BoxButtonTop = styled(Box)`
    display:block;
    @media (max-width: 960px) {
        display:none;
    }
`;
const BoxMenuDesktop = styled(Box)`
    display:block;
    @media (max-width: 960px) {
        display:none;
    }
`;

const BoxButtonMenuMobile = styled(Box)`
    display:none;
    position: relative;
    @media (max-width: 960px) {
        display:block;
    }
`;
const IconButtonToggler = styled(IconButton)`
    background-color: #FAFAFA;
    color: #000000;
    position: relative;
    right: -5px;
    //position: absolute;
    //z-index: 99999;
`;

const LinkMenu = styled(Link)`
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    color: #000;
    margin:0 35px;
    text-decoration: none;
    &:hover{
        text-decoration: underline;
    }
`;

const LinkMenuButton = styled(Link)`
    text-decoration: none;
    &:hover{
        text-decoration: none;
    }
`;
const LinkMenuTo = styled(LinkTo)`
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    color: #000;
    margin:0 35px;
    text-decoration: none;
    &:hover{
        text-decoration: underline;
    }
`;
const GridFlexEnd = styled(Grid)`
    display: flex;
    align-items: flex-end;
    justify-content: flex-end;
`;
const GridFlexCenter = styled(Grid)`
    display: flex;
    align-items: center;
`;

const ContainerMobileTop = styled(Container)`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-top:40px;
`;
const AvatarLogoMob = styled(Avatar)`
    width: 100%;
    max-width:88px;
    height: auto;
    position: relative;
    object-fit: contain;
`;
const IconButtonTogglerMob = styled(IconButton)`
    padding:0;
`;


const ContainerMobile = styled(Container)`
    display:flex;
    flex-direction:column;
    align-items: center;
    justify-content: flex-end;
    padding-top: 10px;
    padding-bottom: 20px;
`;
const MenuItems = styled(Box)``;
const BoxSoc = styled(Box)`
    margin: 20px auto;
    text-align: center;
    display:flex;
`;
const AvatarSoc = styled(Avatar)`
    margin: 10px 15px;
    width: 20px;
    height: auto;
    &:hover{
        transform: scale(1.1);
    }
`;
const TypographyCopy = styled(Typography)`
    font-family: "GT Walsheim Pro Bold", sans-serif;
    font-size: 10px;
    text-align: left;
    color: #919eab;
    text-align: center;
`;

const LinkMenuMob = styled(Link)`
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    color: #000;
    margin:20px auto;
    text-decoration: none;
    &:hover{
        text-decoration: underline;
    }
`;
const LinkMenuMobTo = styled(LinkTo)`
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    color: #000;
    margin:20px auto;
    text-decoration: none;
    &:hover{
        text-decoration: underline;
    }
`;