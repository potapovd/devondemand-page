import React from "react"
import { Link } from "gatsby"
import styled from "styled-components"

import { Avatar, Box, Container, Link as LinkEx, Typography } from "@material-ui/core"

import logo from "../../../images/logo_dod.svg"

import github from "./img/github.svg"
import twitter from "./img/twitter.svg"
import instagram from "./img/instagram.svg"

export default function Footer() {
    return (
        <ContainerFooter>
            <Link to='/'>
                <AvatarLogo src={ logo } variant='square'/>
            </Link>
            <BoxLinks>
                <LinkPages to='/terms-of-service'>
                    Terms of service
                </LinkPages>
                <LinkPages to='/privacy-policy'>
                    Privacy policy
                </LinkPages>
            </BoxLinks>
            <BoxSoc>
                <LinkEx to='https://github.com/devondemandco'>
                    <AvatarSoc src={ github } variant='square'/>
                </LinkEx>
                <LinkEx to='https://www.instagram.com/devondemandco/'>
                    <AvatarSoc src={ instagram } variant='square'/>
                </LinkEx>
                <LinkEx to='https://twitter.com/devondemandco'>
                    <AvatarSoc src={ twitter } variant='square'/>
                </LinkEx>
            </BoxSoc>
            <TypographyCopy>
                &copy; 2020, DEVONDEMAND.CO. All Rights Reserved.
            </TypographyCopy>
        </ContainerFooter>
    )
}

const ContainerFooter = styled(Container)`
    text-align:center;
    display:flex;
    flex-direction: column;
    align-items: center;
`;
const AvatarLogo = styled(Avatar)`
    width: 100%;
    max-width: 135px;
    height: auto;
    margin: 40px auto;
    transition: all .3s;
    &:hover{
        transform: scale(1.1);
    }
`;
const BoxLinks = styled(Box)``;
const LinkPages = styled(Link)`
    text-align: center;
    margin: 10px 10px;
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    color: #000;
    text-decoration: none;
    &:hover{
        text-decoration: underline;
    }
`;
const BoxSoc = styled(Box)`
    margin: 40px auto;
    text-align: center;
    display:flex;
`;
const AvatarSoc = styled(Avatar)`
    margin: 10px 25px;
    width: 25px;
    height: auto;
    transition: all .3s;
    &:hover{
        transform: scale(1.1);
    }
`;
const TypographyCopy = styled(Typography)`
    font-family: "GT Walsheim Pro Bold", sans-serif;
    font-size: 10px;
    color: #919eab;
    text-align: center;
    margin-bottom: 80px;
`;