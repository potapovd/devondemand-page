import React, { Component } from "react"
import "aos/dist/aos.css"
import AOS from "aos"
import styled from "styled-components"

import { Typography } from "@material-ui/core"

const TypographyHeader = styled(Typography)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 30px;
    letter-spacing: 0.03em;
    color: #000;
    text-align: center;
    max-width:760px;
    margin: 40px auto;
    @media (max-width: 960px) {
        font-size: 20px;
        margin: 30px auto;
    }
    @media (max-width: 767px) {
        font-size: 15px;
        margin: 20px auto;
    }
`;


class HeaderSm extends Component {
    componentDidMount() {
        AOS.init({duration: 700})
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }

    render() {
        return (
            <TypographyHeader variant="h4" component="h4" data-aos='fade-down'>
                { this.props.text }
            </TypographyHeader>
        )
    }
}

export default HeaderSm;

