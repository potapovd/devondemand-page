import React, { Component } from "react"
import "aos/dist/aos.css"
import AOS from "aos"
import styled from "styled-components"

import { Typography } from "@material-ui/core"

const TypographyText = styled(Typography)`
    font-family: "GT Walsheim Pro Medium", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    color: #919eab;
    opacity: 0.8;
    line-height: 23px;
    padding: 20px 0;
    text-align: left;
    @media (max-width: 960px) {
        font-size: 16px;
    }
    @media (max-width: 767px) {
        font-size: 14px;
    }
`;

class SubheaderLeft extends Component {
    componentDidMount() {
        AOS.init({duration: 700})
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }

    render() {
        return (
            <TypographyText data-aos='fade-in'>
                { this.props.text }
            </TypographyText>
        )
    }
}

export default SubheaderLeft;

