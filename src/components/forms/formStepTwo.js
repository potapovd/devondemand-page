import React, { useState } from "react"
import styled from "styled-components"

import PropTypes from "prop-types"
import { Form, Formik } from "formik"

// import Checkbox from '@material-ui/core/Checkbox'
// import Checkbox from '@material-ui/core/Checkbox'
// import CircleCheckedFilled from '@material-ui/icons/CheckCircle'
// import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked'
import Header from "../typography/header";

import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Avatar from "@material-ui/core/Avatar";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import CircleCheckedFilled from "@material-ui/icons/CheckCircle";
import CircleUnchecked from "@material-ui/icons/RadioButtonUnchecked";
import Typography from "@material-ui/core/Typography"
import * as yup from "yup"

import stipeLogo from "../../images/stripe.svg"

const validationSchema = yup.object({});

export const FormStepTwo = ({formData, setFormData, prevStep}) => {
    const [ direction, setDirection ] = useState("back");
    return (
        <>
            <Formik
                initialValues={ formData }
                //isInitialValid={validationSchema.isValidSync(formData)}
                enableReinitialize={ true }
                validateOnMount={ true }
                onSubmit={ values => {
                    setFormData(values);
                    if ( direction === "back" ) {
                        prevStep()
                    } else {
                        setTimeout(() => {
                            alert(JSON.stringify(values, null, 2));
                        }, 500);
                    }
                } }
                validationSchema={ validationSchema }
            >
                { props => {
                    return (
                        <Form>
                            <Header text='Let’s bring your design to life'/>
                            <ButtonBack
                                type='submit'
                                variant='contained'
                                color='primary'
                                onClick={ () => setDirection("back") }
                            >
                                Previous step
                            </ButtonBack>
                            <BoxFormGroup>
                                <FormGroupStyled>
                                    <FormControlLabelStyled
                                        control={ <Checkbox size='medium' color='primary' icon={ <CircleUnchecked/> }
                                                            checkedIcon={ <CircleCheckedFilled/> } name="checkedH"/> }
                                        label=""
                                    />
                                    <TypographyName variant="body2">Connect Stripe</TypographyName>
                                    <TypographyDays variant="body2">+2 business days</TypographyDays>
                                    <AvatarLogoBox>
                                        <AvatarLogo src={ stipeLogo } variant='square'/>
                                    </AvatarLogoBox>
                                </FormGroupStyled>
                                <FormGroupStyled>
                                    <FormControlLabelStyled
                                        control={ <Checkbox size='medium' color='primary' icon={ <CircleUnchecked/> }
                                                            checkedIcon={ <CircleCheckedFilled/> } name="checkedH"/> }
                                        label=""
                                    />
                                    <TypographyName variant="body2">Connect Stripe</TypographyName>
                                    <TypographyDays variant="body2">+2 business days</TypographyDays>
                                    <AvatarLogoBox>
                                        <AvatarLogo src={ stipeLogo } variant='square'/>
                                    </AvatarLogoBox>
                                </FormGroupStyled>
                                <FormGroupStyled>
                                    <FormControlLabelStyled
                                        control={ <Checkbox size='medium' color='primary' icon={ <CircleUnchecked/> }
                                                            checkedIcon={ <CircleCheckedFilled/> } name="checkedH"/> }
                                        label=""
                                    />
                                    <TypographyName variant="body2">Connect Stripe</TypographyName>
                                    <TypographyDays variant="body2">+2 business days</TypographyDays>
                                    <AvatarLogoBox>
                                        <AvatarLogo src={ stipeLogo } variant='square'/>
                                    </AvatarLogoBox>
                                </FormGroupStyled>
                                <FormGroupStyled>
                                    <FormControlLabelStyled
                                        control={ <Checkbox size='medium' color='primary' icon={ <CircleUnchecked/> }
                                                            checkedIcon={ <CircleCheckedFilled/> } name="checkedH"/> }
                                        label=""
                                    />
                                    <TypographyName variant="body2">Connect Stripe</TypographyName>
                                    <TypographyDays variant="body2">+2 business days</TypographyDays>
                                    <AvatarLogoBox>
                                        <AvatarLogo src={ stipeLogo } variant='square'/>
                                    </AvatarLogoBox>
                                </FormGroupStyled>
                                <FormGroupStyled>
                                    <FormControlLabelStyled
                                        control={ <Checkbox size='medium' color='primary' icon={ <CircleUnchecked/> }
                                                            checkedIcon={ <CircleCheckedFilled/> } name="checkedH"/> }
                                        label=""
                                    />
                                    <TypographyName variant="body2">Connect Stripe</TypographyName>
                                    <TypographyDays variant="body2">+2 business days</TypographyDays>
                                    <AvatarLogoBox>
                                        <AvatarLogo src={ stipeLogo } variant='square'/>
                                    </AvatarLogoBox>
                                </FormGroupStyled>
                                <FormGroupStyled>
                                    <FormControlLabelStyled
                                        control={ <Checkbox size='medium' color='primary' icon={ <CircleUnchecked/> }
                                                            checkedIcon={ <CircleCheckedFilled/> } name="checkedH"/> }
                                        label=""
                                    />
                                    <TypographyName variant="body2">Connect Stripe</TypographyName>
                                    <TypographyDays variant="body2">+2 business days</TypographyDays>
                                    <AvatarLogoBox>
                                        <AvatarLogo src={ stipeLogo } variant='square'/>
                                    </AvatarLogoBox>
                                </FormGroupStyled>
                                <FormGroupStyled>
                                    <FormControlLabelStyled
                                        control={ <Checkbox size='medium' color='primary' icon={ <CircleUnchecked/> }
                                                            checkedIcon={ <CircleCheckedFilled/> } name="checkedH"/> }
                                        label=""
                                    />
                                    <TypographyName variant="body2">Connect Stripe</TypographyName>
                                    <TypographyDays variant="body2">+2 business days</TypographyDays>
                                    <AvatarLogoBox>
                                        <AvatarLogo src={ stipeLogo } variant='square'/>
                                    </AvatarLogoBox>
                                </FormGroupStyled>
                                <FormGroupStyled>
                                    <FormControlLabelStyled
                                        control={ <Checkbox size='medium' color='primary' icon={ <CircleUnchecked/> }
                                                            checkedIcon={ <CircleCheckedFilled/> } name="checkedH"/> }
                                        label=""
                                    />
                                    <TypographyName variant="body2">Connect Stripe</TypographyName>
                                    <TypographyDays variant="body2">+2 business days</TypographyDays>
                                    <AvatarLogoBox>
                                        <AvatarLogo src={ stipeLogo } variant='square'/>
                                    </AvatarLogoBox>
                                </FormGroupStyled>
                                <FormGroupStyled>
                                    <FormControlLabelStyled
                                        control={ <Checkbox size='medium' color='primary' icon={ <CircleUnchecked/> }
                                                            checkedIcon={ <CircleCheckedFilled/> } name="checkedH"/> }
                                        label=""
                                    />
                                    <TypographyName variant="body2">Connect Stripe</TypographyName>
                                    <TypographyDays variant="body2">+2 business days</TypographyDays>
                                    <AvatarLogoBox>
                                        <AvatarLogo src={ stipeLogo } variant='square'/>
                                    </AvatarLogoBox>
                                </FormGroupStyled>
                                <FormGroupStyled>
                                    <FormControlLabelStyled
                                        control={ <Checkbox size='medium' color='primary' icon={ <CircleUnchecked/> }
                                                            checkedIcon={ <CircleCheckedFilled/> } name="checkedH"/> }
                                        label=""
                                    />
                                    <TypographyName variant="body2">Connect Stripe</TypographyName>
                                    <TypographyDays variant="body2">+2 business days</TypographyDays>
                                    <AvatarLogoBox>
                                        <AvatarLogo src={ stipeLogo } variant='square'/>
                                    </AvatarLogoBox>
                                </FormGroupStyled>
                            </BoxFormGroup>

                            <ButtonSend
                                type='submit'
                                variant='contained'
                                color='primary'
                                //className={classes.button}
                                onClick={ () => setDirection("forward") }
                            >
                                GET STARTED
                            </ButtonSend>
                        </Form>
                    );
                } }
            </Formik>
        </>
    );
};

const ButtonBack = styled(Button)`
    font-family: "GT Walsheim Pro", sans-serif;
    text-decoration: underline;
    font-size: 14px;
    letter-spacing: 0.01em;
    line-height: 20px;
    color: #000;
    text-transform: capitalize;
    padding:0;
    margin: 10px;
    display: inline-block;
    min-width: auto;
    background: none;
    box-shadow:none;
    &:hover{
        background: none;
        box-shadow:none;
        text-decoration: underline;
    }
`;
const ButtonSend = styled(Button)`
    width: 100%;
    border-radius: 8px;
    background: #4B6FFF;
    font-family: "GT Walsheim Pro Bold", sans-serif;
    font-size: 16px;
    letter-spacing: 0.01em;
    line-height: 21px;
    text-align: center;
    color: #fff;
    padding: 20px 20px;
    margin-top: 20px;
    text-transform: capitalize;
    &:disabled {
        background: #dddddd;
        color: #777;
    }
    &:hover{
        background: #3854C3;
    }
    @media (max-width: 768px) {
        font-size: 10px;
        padding: 10px 10px;
    }
`;
const BoxFormGroup = styled(Box)`
    max-height: 510px;
    overflow-y: auto;
    padding-right: 5px;
    padding-left: 10px;
`;
const FormGroupStyled = styled(FormGroup)`
    display: flex;
    flex-direction: row;
    align-items: center;
    width:100%;
    margin: 20px 0;
`;

const FormControlLabelStyled = styled(FormControlLabel)`
    margin-right: 0;
`;
const TypographyName = styled(Typography)`
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    letter-spacing: 0.01em;
`;
const TypographyDays = styled(Typography)`
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    letter-spacing: 0.01em;
    line-height: 21px;
    margin: 0 10px;
    color: #000;
    opacity: 0.5;
`;

const AvatarLogoBox = styled(Box)`
    width: 45px;
    height: 45px;
    background: #fff;
    display: flex;
    justify-content: center;
    align-items: center;
    filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.16));
    border-radius: 50%;
    margin-left: auto;
    justify-self: flex-end;
    @media (max-width: 960px) {
        width: 23px;
        height: 23px;
    }
`;
const AvatarLogo = styled(Avatar)`
    width: 30px;
    height: auto;
    @media (max-width: 960px) {
        width: 15px;
    }
`;

FormStepTwo.propTypes = {
    formData: PropTypes.object.isRequired,
    setFormData: PropTypes.func.isRequired,
    nextStep: PropTypes.func.isRequired,
    prevStep: PropTypes.func.isRequired
};