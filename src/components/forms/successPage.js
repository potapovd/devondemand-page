import React, { Component } from "react"
import styled from "styled-components"

//import PropTypes from "prop-types"
import { Form, Formik, useField } from "formik"
import Box from "@material-ui/core/Box"

import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";


import Typography from "@material-ui/core/Typography"
import * as yup from "yup"
import StartForm from "../buttons/startForm";
import arrow from "./img/arrow.svg";


const validationSchema = yup.object({
    // option: yup
    //     .string()
    //     .ensure()
    //     .required("This field is required")
    //     .max(50),
    // link: yup
    //     .string()
    //     .url("Link is invalid")
    //     .max(200),
    // comment: yup
    //     .string()
    //     .max(400),
    type: yup.string().oneOf([ "partialyUpdated", "newWebsite" ]).required()
    // gender1: yup.array().required('At least one checkbox is required'),
    // mail: yup
    //     .string()
    //     .email("Invalid email")
    //     .required("Email is required")
    //     .max(50)
});

const MyTextInput = ({label, ...props}) => {
    const [ field, meta ] = useField(props);
    return (
        <>
            <LabelText htmlFor={ props.id || props.name }>{ label }</LabelText>
            <StyledInput { ...field } { ...props }  />
            { meta.touched && meta.error ? (
                <StyledErrorMessage>{ meta.error }</StyledErrorMessage>
            ) : null }
        </>
    );
};
const RadioGroupOut = ({label, ...props}) => {
    const [ field, meta ] = useField(props);
    return (
        <>
            <LabelText htmlFor={ props.id || props.name }>{ label }</LabelText>
            <RadioGroupStyed { ...field } { ...props } />
            { meta.touched && meta.error ? (
                <StyledErrorMessage>{ meta.error }</StyledErrorMessage>
            ) : null }
        </>
    );
};

const MySelect = ({label, ...props}) => {
    const [ field, meta ] = useField(props);
    return (
        <>
            <LabelText htmlFor={ props.id || props.name }>{ label }</LabelText>
            <StyledSelect { ...field } { ...props } />
            { meta.touched && meta.error ? (
                <StyledErrorMessage>{ meta.error }</StyledErrorMessage>
            ) : null }
        </>
    );
};

export default class SuccessForm extends Component {

    render() {
        const formData = {};
        //let {isValid} = props

        return (
            <BoxFormOut>
                <Formik
                    initialValues={ formData }
                    enableReinitialize={ true }
                    validateOnMount={ true }
                    onSubmit={ values => {

                        // setFormData(values);
                        setTimeout(() => {
                            alert(JSON.stringify(values, null, 2));
                        }, 500);

                    } }
                    validationSchema={ validationSchema }
                >
                    { props => {
                        let {isValid} = props;
                        return (
                            <Form>
                                <LabelText htmlFor="link">You are looking for*</LabelText>
                                <MySelect name="option">
                                    <option defaultValue value=''>Select option...</option>
                                    <option value="react">React Only</option>
                                    <option value="react_api">React + API</option>
                                    <option value="reactNat">React Native (Mobile) Only</option>
                                    <option value="reactNat_api">React Native (Mobile) + Api</option>
                                </MySelect>
                                <Box>
                                    <RadioGroupOut name='type' label='Are you developing?' value={ undefined }>
                                        <FormControlLabel value="newWebsite" control={ <Radio/> }
                                                          label="A new website"/>
                                        <FormControlLabel value="partialyUpdated" control={ <Radio/> }
                                                          label="Partialy updated website"/>
                                    </RadioGroupOut>
                                </Box>
                                <MyTextInput
                                    label="Link to your API schema (if you have one)"
                                    name="link"
                                    type="text"
                                    placeholder='https://www.myapi.com'
                                    value={ undefined }
                                />
                                <MyTextInput
                                    label="Any comment?"
                                    name="comment"
                                    type="text"
                                    placeholder='You can write a comment here'
                                    value={ undefined }
                                />
                                <StartForm
                                    type='submit'
                                    variant='contained'
                                    text='GET STARTED'
                                    disabled={ !isValid }
                                />
                            </Form>
                        );
                    } }
                </Formik>
            </BoxFormOut>
        )
    }

}


const RadioGroupStyed = styled(RadioGroup)`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    flex-wrap: nowrap;
    
    & label{
    width: 50%;
    margin: 0;
        & .MuiFormControlLabel-label{
            display: flex;
            justify-content: center;
            align-items: center;
            white-space: nowrap;
            border-radius: 8px;
            background: #fafafa;
            font-family: "GT Walsheim Pro Medium", sans-serif;
            text-align: center;
            border: 3px solid #fff;
            color: #7D7D7D;
            width: 100%;
            padding: 15px 45px;
            @media (max-width: 960px) {
                font-size: 11px;
            }
        }
    }

& .Mui-checked,
& .MuiIconButton-root {
    display: none;
}
& .Mui-checked + span  {
    background: #f2f5ff;
    border: 3px solid #2a62ff;
    font-family: "GT Walsheim Pro Medium", sans-serif;
    color: #2a62ff;
}
`
const BoxFormOut = styled(Box)`
    width: 100%;
    border-radius: 40px;
    background: #fff;
    box-shadow: 0px 3px 32px rgba(0, 0, 0, 0.14);
    padding: 55px 75px;
    @media (max-width: 960px) {
        border-radius: 20px;
        padding: 20px 25px;
    }
`;

const StyledInput = styled.input`
    outline: none;
    width: 100%;
    border: none;
    background: #fafafa;
    padding: 19px 25px;
    border-radius: 8px;
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    letter-spacing: 0.01em;
    line-height: 20px;
    color: #000;
    @media (max-width: 960px) {
        padding: 10px 15px;
    }
`;
const StyledSelect = styled.select`
    outline: none;
    width: 100%;
    border: none;
    padding: 19px 25px;
    border-radius: 8px;
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    letter-spacing: 0.01em;
    line-height: 20px;
    color: #000;
    appearance:none;
    background-color: #fafafa;
    background-image: url(${ arrow });
    background-repeat: no-repeat;
    background-position-x: 98%;
    background-position-y: 50%;
    & option {
        /* appearance:none; */
    }
    @media (max-width: 960px) {
        padding: 10px;
    }
`;

const StyledErrorMessage = styled.div`
    color: #f44336;
    margin-left: 14px;
    margin-right: 14px;
`;

const LabelText = styled(Typography)`
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    letter-spacing: 0.01em;
    text-align: left;
    color: #000;
    margin: 15px 0;
`;

