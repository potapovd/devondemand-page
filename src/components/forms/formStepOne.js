import React from "react"
import styled from "styled-components"

import PropTypes from "prop-types"
import { Form, Formik, useField } from "formik"
import Typography from "@material-ui/core/Typography"
import * as yup from "yup"

import Header from "../typography/header";
import StartForm from "../buttons/startForm";
import arrow from "./img/arrow.svg";


const validationSchema = yup.object({
    option: yup
        .string()
        .ensure()
        .required("This field is required")
        .max(50),
    link: yup
        .string()
        .url("Link is invalid")
        .required("This field is required")
        .max(200),
    pages: yup
        .number()
        .required("This field is required")
        .min(1)
        .max(99),
    mail: yup
        .string()
        .email("Invalid email")
        .required("Email is required")
        .max(50)
});

const MyTextInput = ({label, ...props}) => {
    const [ field, meta ] = useField(props);
    return (
        <>
            <LabelText htmlFor={ props.id || props.name }>{ label }</LabelText>
            <StyledInput { ...field } { ...props } />
            { meta.touched && meta.error ? (
                <StyledErrorMessage>{ meta.error }</StyledErrorMessage>
            ) : null }
        </>
    );
};

const MySelect = ({label, ...props}) => {
    const [ field, meta ] = useField(props);
    return (
        <>
            <LabelText htmlFor={ props.id || props.name }>{ label }</LabelText>
            <StyledSelect { ...field } { ...props } />
            { meta.touched && meta.error ? (
                <StyledErrorMessage>{ meta.error }</StyledErrorMessage>
            ) : null }
        </>
    );
};

export const FormStepOne = ({formData, setFormData, nextStep}) => {
    return (
        <>
            <Formik
                initialValues={ formData }
                //isInitialValid={ validationSchema.isValidSync(formData) }
                enableReinitialize={ true }
                //validateOnMount={ true }
                onSubmit={ values => {

                    setFormData(values);
                    if (
                        values.option === "react" ||
                        values.option === "reactNat"
                    ) {
                        setTimeout(() => {
                            alert(JSON.stringify(values, null, 2));
                        }, 500);
                    } else {
                        nextStep();
                    }

                } }
                validationSchema={ validationSchema }
            >
                { props => {
                    let {isValid} = props;
                    return (
                        <Form>
                            <Header text='Let’s bring your design to life'/>
                            <LabelText htmlFor="link">You are looking for*</LabelText>
                            <MySelect name="option">
                                <option defaultValue value=''>Select option...</option>
                                <option value="react">React Only</option>
                                <option value="react_api">React + API</option>
                                <option value="reactNat">React Native (Mobile) Only</option>
                                <option value="reactNat_api">React Native (Mobile) + Api</option>
                            </MySelect>
                            {/* <LabelText for="link">Link to your design*</LabelText> */ }
                            <MyTextInput
                                label="Link to your design*"
                                name="link"
                                type="text"
                                placeholder='https://www.figmaorsketch.com/yourdesign'
                            />
                            <MyTextInput
                                label="Number of pages*"
                                name="pages"
                                type="number"
                                placeholder='3'
                            />
                            <MyTextInput
                                label="Your e-mail*"
                                name="mail"
                                type="email"
                                placeholder='name@email.com'
                            />
                            <StartForm
                                type='submit'
                                variant='contained'
                                text='GET STARTED'
                                disabled={ !isValid }
                            />
                        </Form>
                    );
                } }
            </Formik>
        </>
    );
    //}
};

const StyledInput = styled.input`
  outline: none;
    width: 100%;
    border: none;
    background: #fff;
    padding: 19px 25px;
    border-radius: 8px;
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    letter-spacing: 0.01em;
    line-height: 20px;
    color: #000;
    @media (max-width: 960px) {
        padding: 10px 15px;
    }
`;
const StyledSelect = styled.select`
    outline: none;
    width: 100%;
    border: none;
    padding: 19px 25px;
    border-radius: 8px;
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    letter-spacing: 0.01em;
    line-height: 20px;
    color: #000;
    appearance:none;
    background-color: #fff;
    background-image: url(${ arrow });
    background-repeat: no-repeat;
    background-position-x: 98%;
    background-position-y: 50%;
    & option {
        /* appearance:none; */
    }
    @media (max-width: 960px) {
        padding: 10px;
    }
`;

const StyledErrorMessage = styled.div`
    color: #f44336;
    margin-left: 14px;
    margin-right: 14px;
`;

const LabelText = styled(Typography)`
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    letter-spacing: 0.01em;
    text-align: left;
    color: #000;
    margin: 15px 0;
`;


FormStepOne.propTypes = {
    formData: PropTypes.object.isRequired,
    setFormData: PropTypes.func.isRequired,
    nextStep: PropTypes.func.isRequired
};