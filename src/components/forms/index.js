import React, { useState } from "react";
import { Link } from "gatsby"
import styled from "styled-components"
import { Box, Typography } from "@material-ui/core"

import { FormStepOne } from "./formStepOne";
import { FormStepTwo } from "./formStepTwo";
import { Success } from "./Success";

export default function Form() {
    const [ step, setStep ] = useState(1);
    const [ formData, setFormData ] = useState({
        option: "",
        link: "",
        pages: "",
        mail: "",
        api: "",
    });
    const nextStep = () => setStep(prev => prev + 1);
    const prevStep = () => setStep(prev => prev - 1);
    switch (step) {
        case 1:
            return (
                <Box>
                    <BoxForm>
                        <FormStepOne
                            formData={ formData }
                            setFormData={ setFormData }
                            nextStep={ nextStep }
                            isValid={ false }
                        />
                    </BoxForm>
                    <TypographyCopyrights variant="body1" color="initial">
                        By clicking on the button, you agree to our&nbsp;
                        <LinkCopyrights to="/terms-of-service">Terms of Service</LinkCopyrights>&nbsp;
                        and have read and acknowledge our&nbsp;
                        <LinkCopyrights to="/privacy-policy">Privacy Policy</LinkCopyrights>.
                    </TypographyCopyrights>
                </Box>
            );
        case 2:
            return (
                <Box>
                    <BoxForm>
                        <FormStepTwo
                            formData={ formData }
                            setFormData={ setFormData }
                            nextStep={ nextStep }
                            prevStep={ prevStep }
                        />
                    </BoxForm>
                    <TypographyCopyrights variant="body1" color="initial">
                        By clicking on the button, you agree to our&nbsp;
                        <LinkCopyrights to="/terms-of-service">Terms of Service</LinkCopyrights>&nbsp;
                        and have read and acknowledge our&nbsp;
                        <LinkCopyrights to="/privacy-policy">Privacy Policy</LinkCopyrights>.
                    </TypographyCopyrights>
                </Box>
            );
        default:
            return <Success/>;
    }
};


const BoxForm = styled(Box)`
    width:100%;
    border-radius: 10px;
    background: #e0eaf5;
    margin: 0 auto;
    padding:20px 30px;
`;

const TypographyCopyrights = styled(Typography)`
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 14px;
    line-height: 1.3em;
    text-align: center;
    color: #7D7D7D;
    max-width: 392px;
    margin: 20px auto;
    @media (max-width: 960px) {
        font-size: 10px;
        max-width:100%;
    }
    @media (max-width: 768px) {
        font-size: 9px;
    }
`;
const LinkCopyrights = styled(Link)`
    font-family: "GT Walsheim Pro", sans-serif;
    color: #7D7D7D;
    font-size: 14px;
    line-height: 1.3em;
    @media (max-width: 960px) {
        font-size: 10px;
        max-width:100%;
    }
    @media (max-width: 768px) {
        font-size: 9px;
    }
`;