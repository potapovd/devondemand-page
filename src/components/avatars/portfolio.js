import React, { Component } from "react"
import "aos/dist/aos.css"
import AOS from "aos"

import styled from "styled-components"

import { Avatar, Container } from "@material-ui/core"
import img from "../../images/portfolio.jpg"

export default class portfolioAvatar extends Component {
    componentDidMount() {
        AOS.init({duration: 700})
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }

    render() {
        return (
            <Container disableGutters maxWidth={ false }>
                <AvatarPhoto src={ img } variant='square' data-aos='fade-down' data-aos-delay='50'/>
            </Container>
        )
    }
}

const AvatarPhoto = styled(Avatar)`
    width: 100%;
    height: 90vh;
    margin: 200px auto;
    @media (max-width: 960px) {
        margin: 70px auto;
    }
`;
