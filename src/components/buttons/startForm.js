import React, { Component } from "react"
import "aos/dist/aos.css"
import AOS from "aos"
import styled from "styled-components"

import { Button } from "@material-ui/core"

class startForm extends Component {

    componentDidMount() {
        AOS.init({duration: 700})
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }

    render() {
        return (
            <ButtonForm type='submit' disabled={ this.props.disabled } data-aos='zoom-in'>
                { this.props.text }
            </ButtonForm>
        )
    }
}

export default startForm;

const ButtonForm = styled(Button)`
    width: 100%;
    border-radius: 8px;
    background: #4B6FFF;
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 20px;
    letter-spacing: 0.01em;
    line-height: 21px;
    text-align: center;
    color: #fff;
    padding: 20px 20px;
    margin-top: 20px;
    text-transform: capitalize;
    &:disabled {
        background: #dddddd;
        color: #777;
    }
    &:hover{
        background: #3854C3;
    }
    @media (max-width: 768px) {
        font-size: 10px;
        padding: 10px 10px;
    }
`;