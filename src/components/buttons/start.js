import React, { Component } from "react"
import "aos/dist/aos.css"
import AOS from "aos"
import styled from "styled-components"

import { Button } from "@material-ui/core"

class StartSm extends Component {

    componentDidMount() {
        AOS.init({duration: 700})
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }

    render() {
        return (
            <ButtonSmall ref="formSection" data-aos='zoom-in'>
                { this.props.text }
            </ButtonSmall>
        )
    }
}

export default StartSm;

const ButtonSmall = styled(Button)`
    padding: 10px 20px;
    border-radius: 6px;
    background: #4b6fff;
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    text-align: center;
    color: #fff;
    text-transform: capitalize;
    &:hover{
        background: #3854C3;
    }
    @media (max-width: 768px) {
        font-size: 11px;
    }
`;