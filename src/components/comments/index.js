import React, { Component } from "react"
import styled from "styled-components"

import "aos/dist/aos.css"
import AOS from "aos"

import { Avatar, Box, Typography } from "@material-ui/core"
import star from "../blocks/join/img/star1.svg"

import Flipcard from "@kennethormandy/react-flipcard"
import "@kennethormandy/react-flipcard/dist/Flipcard.css"


export default class Comm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            flipped: !props.isClosed,
            isDesktop: false
        }
        this.handleResize = this.handleResize.bind(this);
    }

    handleResize() {
        this.setState({isDesktop: window.innerWidth > 960});
    }

    componentDidMount() {
        AOS.init({duration: 700})
        this.handleResize();
        window.addEventListener("resize", this.handleResize);
    }

    componentWillReceiveProps() {
        AOS.refresh();
    }


    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize);
    }


    render() {
        let isDesktop = this.state.isDesktop;
        let styleFix;

        if ( isDesktop ) {
            styleFix = {zIndex: "99", transition: "all  1s"};
        } else {
            styleFix = {zIndex: "99", left: "20px", transition: "all  1s", right: "auto"};
        }
        return (

            <FlipcardStyled

                style={ this.state.flipped ? styleFix : {zIndex: "-9", transition: "all  1s"} }
                flipped={ this.state.flipped } type="horizontal">
                <BoxCommOpen onClick={ e => this.setState({flipped: !this.state.flipped}) }>
                    <BoxCommStars>
                        <ImgStarLg src={ star } alt="star"/>
                        <ImgStarLg src={ star } alt="star"/>
                        <ImgStarLg src={ star } alt="star"/>
                        <ImgStarLg src={ star } alt="star"/>
                        <ImgStarLg src={ star } alt="star"/>
                    </BoxCommStars>
                </BoxCommOpen>
                <BoxCommClosed onClick={ e => this.setState({flipped: !this.state.flipped}) }>
                    <BoxNameAndStar>
                        <TypographyName>{ this.props.name }</TypographyName>
                        <BoxCommStarsName>
                            <ImgStarSm src={ star } alt="star"/>
                            <ImgStarSm src={ star } alt="star"/>
                            <ImgStarSm src={ star } alt="star"/>
                            <ImgStarSm src={ star } alt="star"/>
                            <ImgStarSm src={ star } alt="star"/>
                        </BoxCommStarsName>
                    </BoxNameAndStar>
                    <TypographyComm color="initial">{ this.props.text }</TypographyComm>
                </BoxCommClosed>
            </FlipcardStyled>
        )
    }
};


const FlipcardStyled = styled(Flipcard)``;

const BoxCommOpen = styled(Box)`
    padding: 20px;
    width:100%;
    max-width:350px;
    /* max-width: 450px; */
    border-radius: 9px;
    background: #fff;
    box-shadow: 0 0 25px rgba(0, 0, 0, 0.14);
    position: relative;
    z-index: 100;
    @media (max-width: 960px) {
        width:100%;
        padding: 10px;
        /* max-width:200px; */
    }
`;
const BoxCommClosed = styled(Box)`
    padding: 20px 40px;
    /* max-width: 360px; */
    width:auto;
    display:inline-block;
    border-radius: 9px;
    background: #F6F7FA;
    box-shadow: 0 0 25px rgba(0, 0, 0, 0.14);
    position: relative;
    z-index: 9;
    @media (max-width: 960px) {
        padding:15px 30px;
    }
    @media (max-width: 767px) {
        padding:10px 20px;
    }
`;
const TypographyName = styled(Typography)`
    font-family: "GT Walsheim Pro Bold", sans-serif;
    font-size: 14px;
    letter-spacing: 0.03em;
    line-height: 13px;
    color: #000;
    position: relative;
    top: -4px;
    margin-right: 10px;
    @media (max-width: 960px) {
        font-size: 12px;
    }
    @media (max-width: 767px) {
        font-size: 10px;
    }
`;
const BoxCommStarsName = styled(Box)``;

const BoxNameAndStar = styled(Box)`
    display:flex;
    flex-direction: row;
    align-items: center;
`;
const BoxCommStars = styled(Box)`
    text-align:center;
    display:flex;
    align-items:center;
    justify-content: space-around;
`;
const TypographyComm = styled(Typography)`
    font-family: "GT Walsheim Pro Medium", sans-serif;
    font-size: 18px;
    letter-spacing: 0.03em;
    line-height: 1.3em;
    color: #000;
    opacity: 0.5;
    margin-top:10px;
    @media (max-width: 960px) {
        font-size: 12px;
    }
    @media (max-width: 767px) {
        font-size: 10px;
    }
`;
const ImgStarSm = styled(Avatar)`
    width: 18px;
    height: auto;
    margin-right:5px;
    display: inline-block;
`;
const ImgStarLg = styled(Avatar)`
    width: 34px;
    height: auto;
    margin-right:5px;
    display: inline-block;
    @media (max-width: 960px) {
        width: 25px;
    }
    @media (max-width: 767px) {
        width: 10px;
    }
`;
