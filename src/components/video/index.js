import React, { Component } from "react"
import { Link } from "gatsby"
import styled from "styled-components"

import { Avatar, Box } from "@material-ui/core"

class Video extends Component {
    render() {
        return (
            <Box justifyContent='center' align='center'>
                <AvatarVideo variant='rounded' src={ this.props.src }/>
                <LinkVideo to={ this.props.url }>
                    { this.props.text }
                </LinkVideo>
            </Box>
        )
    }
}

export default Video;


const AvatarVideo = styled(Avatar)`
    width:100%;
    max-width: 720px;
    height:auto;
`;
const LinkVideo = styled(Link)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    text-align: center;
    color: #fff;
    text-decoration: none;
    display: inline-block;
    padding: 20px;
    border-radius: 36px;
    background: #4b6fff;
    box-shadow: 0 0 25px rgba(0, 0, 0, 0.14);
    position: relative;
    top: -30px;
    &:hover{
        background: #3854C3;
    }
    @media (max-width: 768px) {
        font-size: 11px;
    }
`;
