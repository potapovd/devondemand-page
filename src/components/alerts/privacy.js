import React from "react"
import { Link } from "gatsby"
import styled from "styled-components"

import { Box, Collapse, Grid, IconButton } from "@material-ui/core"
import Alert from "@material-ui/lab/Alert"
import CloseIcon from "@material-ui/icons/Close"

export default function Privacy() {
    const [ open, setOpen ] = React.useState(true);
    return (
        <BoxSticky>
            <GridPrivacy item>
                <Collapse in={ open }>
                    <AlertPrivacy icon={ false } action={
                        <IconButton aria-label="close" color="inherit" size="small" onClick={ () => {
                            setOpen(false);
                        } }>
                            <CloseIcon fontSize="inherit"/>
                        </IconButton>
                    }>
                        By using this website, you agree to our <LinkPrivacy to='/privacy-policy'> cookie
                        policy</LinkPrivacy>
                    </AlertPrivacy>
                </Collapse>
            </GridPrivacy>
        </BoxSticky>
    )
}
const GridPrivacy = styled(Grid)``;
const BoxSticky = styled(Box)`
    position: fixed;
    bottom: 20px;
    left: 20px;
    z-index:100;
    width: 370px;
    @media (max-width: 767px) {
        width: calc(100% - 40px);
        bottom: 10px;
    }
`
const AlertPrivacy = styled(Alert)`
    border: 1px solid #DDE5EC;
    color:#000;
    font-family: "GT Walsheim Pro", sans-serif;
    font-size: 12px;
    background-color:#fff;
    line-height: 21px;
`;

const LinkPrivacy = styled(Link)`
    text-decoration: none;
    color: #4B6FFF;
`;
