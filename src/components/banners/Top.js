import React, { useState } from "react"
import styled from "styled-components"

import { Box, Container, IconButton, Typography } from "@material-ui/core"
import CloseIcon from "@material-ui/icons/Close"

export default function Top() {
    const [ isShowBanner, setShowBanner ] = useState(true);
    return (
        <Box component="header">
            {
                isShowBanner &&
                <GreenBox>
                    <Container>
                        <ContainerInner>
                            <TypographyBanner variant="h6">
                                Product Hunt launch: get a $50 discount until October 02, 2020&nbsp;
                                <Box component="span" role="img" aria-label="Kiss" display="inline">😘</Box>
                            </TypographyBanner>
                            <IconButtonClose aria-label="close" size="small" onClick={ () => {
                                setShowBanner(false)
                            } }>
                                <CloseIcon fontSize="inherit"/>
                            </IconButtonClose>
                        </ContainerInner>
                    </Container>
                </GreenBox>
            }
        </Box>
    )
}

const GreenBox = styled(Box)`
    background-color: #2ef893;
    padding:20px 0;
`;
const ContainerInner = styled(Box)`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;
const IconButtonClose = styled(IconButton)`
    background-color: #FAFAFA;
    color: #000000;
`;

const TypographyBanner = styled(Typography)`
    font-family: "GT Walsheim Pro Bold", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    @media (max-width: 768px) {
        font-size: 10px;
        padding-right:40px
    }
`