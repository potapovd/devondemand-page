import React from "react"
import styled from "styled-components"


import { Accordion, AccordionDetails, AccordionSummary, Box, Typography } from "@material-ui/core"
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

export default function SimpleAccordion(props) {
    return (
        <Box>
            {
                props.questions.map((item, index) => {
                    return (
                        <AccordionStyled key={ `${ item.title }_${ index }` }>
                            <AccordionSummary
                                expandIcon={ <ExpandMoreIcon/> }
                            >
                                <TypographyAccordion
                                    className={ `accordion_title` }>{ item.title }</TypographyAccordion>
                            </AccordionSummary>
                            <AccordionDetails>
                                <TypographyDetails>
                                    { item.text }
                                </TypographyDetails>
                            </AccordionDetails>
                        </AccordionStyled>
                    )
                })
            }
        </Box>
    );
}

const AccordionStyled = styled(Accordion)`
    box-shadow: none;
    background: transparent;
    transition: all .3s;
    &:hover{
      background: #f1f1f1;
    }
`
const TypographyAccordion = styled(Typography)`
    font-family: "GT Walsheim Pro Black", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    text-align: left;
    color: #000;
    margin:20px 0; 
    @media (max-width: 768px) {
        font-size: 12px;
    }
`;
const TypographyDetails = styled(Typography)`
    font-family: "GT Walsheim Pro Medium", sans-serif;
    font-size: 20px;
    letter-spacing: 0.03em;
    line-height: 1.8em;
    text-align: left;
    color: #919eab;
    opacity: 0.8;
    @media (max-width: 768px) {
        font-size: 10px;
    }
`;
