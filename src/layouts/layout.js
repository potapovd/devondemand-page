import React from "react"

import CssBaseline from "@material-ui/core/CssBaseline"
import GlobalStyle from "../styles/global"

import Helmet from "react-helmet"
import favico from "../favicons/favicon.ico"
import fav32 from "../favicons/icon-32x32.png"
import fav128 from "../favicons/icon-128x128.png"
import fav152 from "../favicons/icon-152x152.png"
import fav167 from "../favicons/icon-167x167.png"
import fav180 from "../favicons/icon-180x180.png"
import fav192 from "../favicons/icon-192x192.png"

import metaimage from "../images/seo.png"

export default function Layout({children}) {
    return (
        <React.Fragment>
            <Helmet
                encodeSpecialCharacters={ true }
                htmlAttributes={ {lang: "en"} }
                title='Dev on demand | Devondemand'
                meta={ [
                    {name: "description", content: "Devondemand is a web app to get react development tasks done in a few days.",},
                    {name: "keywords", content: "frontend, developer",},
                    {name: "viewport", content: "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no",},

                    {name: "twitter:site", content: "https://devondemand.co/"},
                    {name: "twitter:creator", content: "BCL"},
                    {name: "twitter:title", content: "Dev on demand"},
                    {name: "twitter:card", content: "summary"},
                    {name: "twitter:description", content: "Devondemand is a web app to get react development tasks done in a few days."},
                    {name: "twitter:image", content: metaimage},

                    {property: "og:title", content: "Dev on demand"},
                    {property: "og:site_name", content: "Dev on demand"},
                    {property: "og:type", content: "website"},
                    {property: "og:url", content: "https://devondemand.co/"},
                    {property: "og:description", content: "Devondemand is a web app to get react development tasks done in a few days."},
                    {property: "og:image", content: metaimage},

                    {name: "apple-mobile-web-app-capable", content: "yes",},
                    {name: "apple-touch-fullscreen", content: "yes",},
                    {name: "apple-mobile-web-app-status-bar-style", content: "default",},
                ] }
                 link={[
                     {rel: "icon", type: "image/png", sizes: "32x32", href: fav32},
                    {rel: "icon", type: "image/png", sizes: "128x128", href: fav128},
                    {rel: "icon", type: "image/png", sizes: "152x152", href: fav152},
                    {rel: "icon", type: "image/png", sizes: "167x167", href: fav167},
                    {rel: "icon", type: "image/png", sizes: "180x180", href: fav180},
                    {rel: "icon", type: "image/png", sizes: "192x192", href: fav192},
                    {rel: "shortcut icon", type: "image/png", href: favico},
                    ]}
            />
            <CssBaseline/>
            <GlobalStyle/>
            { children }
        </React.Fragment>

    )

}