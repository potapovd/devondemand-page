import React from 'react'
import Layout from '../layouts/layout'

import Top from '../components/banners/Top'
import Menu from '../components/blocks/menu'
import Development from '../components/blocks/development/Development'
import PortfolioAvatar from '../components/avatars/portfolio'
import PortfolioPage from '../components/blocks/contnet/portfolio'
import Advantages from '../components/blocks/advantages'
import Footer from '../components/blocks/footer'
import Privacy from '../components/alerts/privacy'

 const Portfolio = () => {
  return (
    <Layout>
      <Top />
      <Menu />
      <Development />
      <PortfolioAvatar />
      <PortfolioPage />
      <Advantages />
      <Footer />
      <Privacy />
    </Layout>
  )
}

export default Portfolio;