import React from 'react'
import Layout from '../layouts/layout'

import Top from '../components/banners/Top'
import Menu from '../components/blocks/menu'
import FaqPage from '../components/blocks/contnet/faq'
import Advantages from '../components/blocks/advantages'
import Footer from '../components/blocks/footer'
import Privacy from '../components/alerts/privacy'

 const Faq = () => {
  return (
    <Layout>
      <Top />
      <Menu />
      <FaqPage />
      <Advantages />
      <Footer />
      <Privacy />
    </Layout>
  )
}

export default Faq;