import React from "react"

import Layout from "../layouts/layout"
import Top from "../components/banners/Top"
import Menu from "../components/blocks/menu"
import DevelopmentAndForm from "../components/blocks/development/DevelopmentAndForm"
import Process from "../components/blocks/process/indexPage"
//import Extensions from '../components/blocks/extensions'
import Join from "../components/blocks/join"
import Price from "../components/blocks/price"
import Notifications from "../components/blocks/notifications"
import Integrate from "../components/blocks/integrate"
import Advantages from "../components/blocks/advantages"
import Footer from "../components/blocks/footer"
import Privacy from "../components/alerts/privacy"


export default function Home() {
    return (
        <Layout>
            <Top/>
            <Menu/>
            <DevelopmentAndForm/>
            <Process/>
            {/*<Extensions />*/ }
            <Join/>
            <Price/>
            <Notifications/>
            <Integrate/>
            <Advantages/>
            <Footer/>
            <Privacy/>
        </Layout>
    )
}