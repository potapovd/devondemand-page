import React from 'react'
import Layout from '../layouts/layout'

import Top from '../components/banners/Top'
import Menu from '../components/blocks/menu'
import PrivacyPolicyPage from '../components/blocks/contnet/privacyPolicy'
import Advantages from '../components/blocks/advantages'
import Footer from '../components/blocks/footer'
import Privacy from '../components/alerts/privacy'

const PrivacyPolicy = () => {
  return (
    <Layout>
      <Top />
      <Menu />
      <PrivacyPolicyPage />
      <Advantages />
      <Footer />
      <Privacy />
    </Layout>
  )
}

export default PrivacyPolicy;