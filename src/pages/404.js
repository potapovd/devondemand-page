import React from "react"

import Layout from "../layouts/layout"
import Top from "../components/banners/Top"
import Menu from "../components/blocks/menu"
import Footer from "../components/blocks/footer"
import Privacy from "../components/alerts/privacy"
import NotFoundPage from "../components/blocks/contnet/notfound";


export default function Home() {
    return (
        <Layout>
            <Top/>
            <Menu/>
            <NotFoundPage />
            <Footer/>
            <Privacy/>
        </Layout>
    )
}