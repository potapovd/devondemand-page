import React from "react"

import Layout from "../layouts/layout"
import SuccessPage from "../components/blocks/contnet/success";



export default function Success() {
    return (
        <Layout>
            <SuccessPage />
        </Layout>
    )
}