import React from 'react'
import Layout from '../layouts/layout'


import Top from '../components/banners/Top'
import Menu from '../components/blocks/menu'
import TermsOfServicePage from '../components/blocks/contnet/termsOfService'
import Advantages from '../components/blocks/advantages'
import Footer from '../components/blocks/footer'
import Privacy from '../components/alerts/privacy'

 const TermsOfService = () => {
  return (
    <Layout>
      <Top />
      <Menu />
      <TermsOfServicePage />
      <Advantages />
      <Footer />
      <Privacy />
    </Layout>
  )
}

export default TermsOfService;