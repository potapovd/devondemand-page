/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
    plugins: [
        "gatsby-transformer-sharp",
        "gatsby-plugin-sharp",
        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "images",
                path: `${ __dirname }/src/images`,
            },
        },
        {
            resolve: "gatsby-plugin-manifest",
            options: {
                name: "Dev on demand | Devondemand",
                short_name: "Devondemand",
                start_url: "/",
                background_color: "#ffffff",
                theme_color: "#ffffff",
                display: "minimal-ui",
                include_favicon: false,
                legacy: false,
            },
        },
        {
            resolve: "gatsby-plugin-material-ui",
            options: {
                stylesProvider: {
                    injectFirst: true,
                },
            },
        },
        "gatsby-plugin-styled-components",
        "gatsby-plugin-react-helmet",
    ],
};